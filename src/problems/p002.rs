//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::EvenFibonacci;

pub const INFO: EulerInfo = EulerInfo {
    number: 2,
    title: "Even fibonacci numbers",
    slug: "even-fibonacci-numbers",
    solve: Some(solve_default),
    solution: Some("$2b$04$X8Wz5p3CfNBBZeWakJAasOL8trbBSOmSdUN6z0mPyJAi1vHgWAvR6"),
};

pub fn solve_default() -> String {
    solve(4_000_000).to_string()
}

pub fn solve(upper_bound: u32) -> u32 {
    // generate a new fibonacci iterator
    EvenFibonacci::new()
        .take_while(|&a|
            // iterate as long as the numbers are
            // smaller than upper_bound
            a < upper_bound)
        .fold(0, |sum, item|
            // add them all up
            sum + item)
}

#[test]
fn test_solve() {
    assert!(solve(3) == 2);
    assert!(solve(4) == 2);
    assert!(solve(5) == 2);
    assert!(solve(6) == 2);
    assert!(solve(7) == 2);
    assert!(solve(8) == 2);
    assert!(solve(9) == 2 + 8);
    assert!(solve(10) == 2 + 8);
    assert!(solve(11) == 2 + 8);
    assert!(solve(12) == 2 + 8);
    assert!(solve(35) == 2 + 8 + 34);
    assert!(solve(40) == 2 + 8 + 34);
    assert!(solve(50) == 2 + 8 + 34);
    assert!(solve(144) == 2 + 8 + 34);
    assert!(solve(145) == 2 + 8 + 34 + 144);
}
