//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 6,
    title: "Sum square difference",
    slug: "sum-square-difference",
    solve: Some(solve_default),
    solution: Some("$2b$04$d1w0.sw18xTTl69Jq0VdLeAz.RyNArU7K2L09md3o/PzDRhOiwlIa"),
};

pub fn solve_default() -> String {
    solve(100).to_string()
}

pub fn solve(n: u32) -> u32 {
    (3 * n.pow(4) + 2 * n.pow(3) - 3 * n.pow(2) - 2 * n) / 12
}

#[test]
fn solve_works() {
    assert!(solve(10) == 2640);
}
