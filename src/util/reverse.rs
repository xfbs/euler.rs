use super::digits::ToDigits;
use std::ops::{Add, Div, DivAssign, Mul, MulAssign, Rem, RemAssign};

/// Trait to generate the reverse of a number.
pub trait Reverse
where
    Self: Sized,
{
    /// Get the reverse of a number in a specific base.
    ///
    /// ## Examples
    ///
    /// ```
    /// use xfbs_euler::util::Reverse;
    ///
    /// assert_eq!(1234.reverse(10), 4321);
    /// ```
    fn reverse(&self, base: Self) -> Self;
}

impl<T> Reverse for T
where
    T: ToDigits<T>
        + Copy
        + DivAssign
        + MulAssign
        + RemAssign
        + Add<Output = T>
        + Div<Output = T>
        + Rem<Output = T>
        + Mul<Output = T>
        + PartialEq
        + PartialOrd
        + From<u8>,
{
    fn reverse(&self, base: Self) -> Self {
        self.digits()
            .base(base)
            .reverse()
            .fold(0u8.into(), |m, c| base as T * m + c)
    }
}

#[test]
fn test_reverse() {
    assert_eq!(12_u8.reverse(10), 21_u8);
    assert_eq!(312_u16.reverse(10), 213_u16);
    assert_eq!(2353_u32.reverse(10), 3532_u32);
    assert_eq!(9523473_u64.reverse(10), 3743259_u64);

    assert_eq!(312_i16.reverse(10), 213_i16);
    assert_eq!(2353_i32.reverse(10), 3532_i32);
    assert_eq!(9523473_i64.reverse(10), 3743259_i64);
}
