//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::factorial;

pub const INFO: EulerInfo = EulerInfo {
    number: 24,
    title: "Lexicographic permutations",
    slug: "lexicographic-permutations",
    solve: Some(solve_default),
    solution: Some("$2b$04$i99UZ59vS0WXnxvGxjB8EePcWsfUlKNud8KpUo/g5ifv3v7kat59m"),
};

pub fn solve_default() -> String {
    solve(1000000).to_string()
}

// FIXME: rewrite using euler::permutations
pub fn solve(n: usize) -> u64 {
    let v = nth_permutation(vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9], n - 1);
    v.iter().fold(0, |m, c| (m * 10) + (*c as u64))
}

fn nth_permutation(mut arr: Vec<u8>, mut n: usize) -> Vec<u8> {
    let mut result = vec![];

    while arr.len() > 0 {
        let permutations = factorial(arr.len() as u64 - 1).unwrap() as usize;
        result.push(arr.remove(n / permutations));
        n %= permutations;
    }

    result
}

#[test]
fn test_nth_permutation() {
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 0), vec![1, 2, 3, 4]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 1), vec![1, 2, 4, 3]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 2), vec![1, 3, 2, 4]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 3), vec![1, 3, 4, 2]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 4), vec![1, 4, 2, 3]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 5), vec![1, 4, 3, 2]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 6), vec![2, 1, 3, 4]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 7), vec![2, 1, 4, 3]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 8), vec![2, 3, 1, 4]);
    assert_eq!(nth_permutation(vec![1, 2, 3, 4], 9), vec![2, 3, 4, 1]);
}

#[test]
fn test_solve() {
    assert_eq!(solve(1), 123456789);
    assert_eq!(solve(2), 123456798);
    assert_eq!(solve(3), 123456879);
    assert_eq!(solve(4), 123456897);
}
