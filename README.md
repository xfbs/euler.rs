# Project Euler Solutions

This is a repository of solutions to [Project Euler](https://projecteuler.net/)
problems. All of these solutions are written by me in an attempt to fool around
with mathematics and sharpen my programming skills in a few languages.

Documentation: [xfbs-euler](https://xfbs.gitlab.io/euler.rs/docs/xfbs_euler/)  
Progress: [Progress](https://xfbs.gitlab.io/euler.rs/progress.html)  
Continuous Integration: [![pipeline status](https://gitlab.com/xfbs/euler.rs/badges/master/pipeline.svg)](https://gitlab.com/xfbs/euler.rs/-/commits/master)  
Snapshot Build: [xfbs-euler-amd64.xz](https://xfbs.gitlab.io/euler.rs/snapshot/xfbs-euler-amd64.xz).

## Rules

  - Idiomatic Rust
  - Needs to take less than 1s (1000ms) to compute the answer.

## Progress

To check the progress, run

    cargo run -- check -a

Which will run the solvers for the known solutions and output information.

## License

See [`LICENSE.md`](license.md) for more information. 
