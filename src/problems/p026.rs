//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 26,
    title: "Reciprocal cycles",
    slug: "reciprocal-cycles",
    solve: Some(solve_default),
    solution: Some("$2b$04$.nqlpR/IxPm4h38Puyb.G.BGY.Dr7esN7JvD240oDJNl1b/AKjEoq"),
};

pub fn solve_default() -> String {
    solve(1000).to_string()
}

pub fn solve(max: u64) -> u64 {
    (2..max)
        .map(|number| (number, fraction_info(number)))
        .filter(|(_, info)| match info {
            FractionInfo::Cycle(_, _) => true,
            _ => false,
        })
        .fold((0, 0), |(number, max), (cur, info)| match info {
            FractionInfo::Cycle(_, x) if x > max => (cur, x),
            _ => (number, max),
        })
        .0
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum FractionInfo {
    Finite(usize),
    Cycle(usize, usize),
    Unknown,
}

fn fraction_info(number: u64) -> FractionInfo {
    let mut number = number;
    let mut twos = 0;
    let mut fives = 0;

    while (number % 2) == 0 {
        twos += 1;
        number /= 2;
    }

    while (number % 5) == 0 {
        fives += 1;
        number /= 5;
    }

    let finite = twos.max(fives);

    if number == 1 {
        return FractionInfo::Finite(finite);
    }

    let mut divisor: u128 = 10;
    let number: u128 = number as u128;

    for length in 1..(number as usize) {
        if divisor % number == 1 {
            return FractionInfo::Cycle(finite, length);
        }

        divisor *= 10;
        divisor %= number;
    }

    FractionInfo::Unknown
}

#[test]
fn test_fraction_info() {
    use FractionInfo::*;

    // finite fractions
    assert_eq!(fraction_info(2), Finite(1));
    assert_eq!(fraction_info(5), Finite(1));
    assert_eq!(fraction_info(4), Finite(2));
    assert_eq!(fraction_info(8), Finite(3));
    assert_eq!(fraction_info(10), Finite(1));
    assert_eq!(fraction_info(16), Finite(4));
    assert_eq!(fraction_info(20), Finite(2));
    assert_eq!(fraction_info(32), Finite(5));
    assert_eq!(fraction_info(40), Finite(3));

    // cyclic fractions
    assert_eq!(fraction_info(3), Cycle(0, 1));
    assert_eq!(fraction_info(7), Cycle(0, 6));
    assert_eq!(fraction_info(9), Cycle(0, 1));
    assert_eq!(fraction_info(11), Cycle(0, 2));
    assert_eq!(fraction_info(17), Cycle(0, 16));
    assert_eq!(fraction_info(33), Cycle(0, 2));
    assert_eq!(fraction_info(99), Cycle(0, 2));

    // cyclic fractions with finite prefix
    assert_eq!(fraction_info(6), Cycle(1, 1));
    assert_eq!(fraction_info(12), Cycle(2, 1));
    assert_eq!(fraction_info(15), Cycle(1, 1));
    assert_eq!(fraction_info(24), Cycle(3, 1));
}
