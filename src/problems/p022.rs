//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 22,
    title: "Names scores",
    slug: "names-scores",
    solve: Some(solve_default),
    solution: Some("$2b$04$KIC8/Qoal0iCF1x8uA8AR.A9Ne6jKA.BV3qwlMh7peZS0XDQUbxeq"),
};

pub const DATA: &'static str = include_str!("p022.txt");

pub fn solve_default() -> String {
    let mut words = DATA
        .split(',')
        .map(|word| {
            let len = word.len();
            word[1..len - 1].to_string()
        })
        .collect::<Vec<String>>();

    solve(&mut words).to_string()
}

pub fn solve(words: &mut Vec<String>) -> usize {
    words.sort_unstable();
    words
        .iter()
        .enumerate()
        .map(|(index, word)| (index + 1) * worth(word))
        .sum()
}

pub fn worth(s: &str) -> usize {
    s.chars().map(|c| c as usize - 'A' as usize + 1).sum()
}

#[test]
fn test_worth() {
    assert_eq!(worth("A"), 1);
    assert_eq!(worth("B"), 2);
    assert_eq!(worth("Z"), 26);
    assert_eq!(worth("COLIN"), 53);
}

#[test]
fn test_solve() {
    assert_eq!(solve(&mut vec!["A".to_string()]), 1);
    assert_eq!(
        solve(&mut vec![
            "A".to_string(),
            "Z".to_string(),
            "COLIN".to_string(),
        ]),
        1 * 1 + 2 * 53 + 3 * 26
    );
}
