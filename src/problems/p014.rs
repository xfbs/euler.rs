//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 14,
    title: "Longest Collatz sequence",
    slug: "longest-collatz-sequence",
    solve: Some(solve_default),
    solution: Some("$2b$04$/2CA1/lkc/cPT47S2cvSzOBsMiNgBBwiS3NvCI5PThj2sGNcKcyBe"),
};

pub fn solve_default() -> String {
    solve(1000000).to_string()
}

struct CollatzLength {
    cache: Vec<u64>,
}

impl CollatzLength {
    fn new(size: usize) -> CollatzLength {
        let mut cache = vec![0; size];
        cache[1] = 1;

        CollatzLength { cache: cache }
    }

    fn len(&mut self, num: u64) -> u64 {
        let mut num = num;
        let mut add = 0;

        while num > 1 && (num % 2) == 0 {
            num /= 2;
            add += 1;
        }

        let cacheable = (num as usize) < self.cache.len();

        if cacheable && self.cache[num as usize] != 0 {
            add + self.cache[num as usize]
        } else {
            let len = 1 + add + self.len(3 * num + 1);

            if cacheable {
                self.cache[num as usize] = len - add;
            }

            len
        }
    }

    fn longest(&mut self, max: u64) -> u64 {
        (1..max)
            .map(|n| (n, self.len(n as u64)))
            .max_by_key(|n| n.1)
            .unwrap()
            .0 as u64
    }
}

pub fn solve(max: u64) -> u64 {
    let mut collatz = CollatzLength::new(max as usize);
    collatz.longest(max)
}

#[test]
fn test_collatz_length_len() {
    let mut cl = CollatzLength::new(10);
    let mut cs = CollatzLength::new(1000);

    for i in 4..30 {
        let num = (2 as u64).pow(i - 1);
        assert_eq!(cl.len(num), i as u64);
        assert_eq!(cs.len(num), i as u64);

        if ((num - 1) % 3) == 0 && (((num - 1) / 3) % 2) == 1 {
            let num = (num - 1) / 3;
            assert_eq!(cl.len(num), i as u64 + 1);
            assert_eq!(cs.len(num), i as u64 + 1);
        }
    }

    let mut cl = CollatzLength::new(10);
    let mut cs = CollatzLength::new(1000);
    let v = vec![1, 2, 4, 8, 16, 5, 10, 20, 40, 13, 26, 52, 17, 34, 11];
    for (i, n) in v.iter().enumerate() {
        assert_eq!(cs.len(*n as u64), (i + 1) as u64);
        assert_eq!(cl.len(*n as u64), (i + 1) as u64);
    }

    for i in 1..500 {
        println!("i is {}", i);
        assert_eq!(cl.len(i), cs.len(i));
    }
}

#[test]
fn test_collatz_longest() {
    let mut cs = CollatzLength::new(1000);
    let maxs = vec![20, 100, 235];

    for max in maxs.iter() {
        let longest = cs.longest(*max);
        let length = cs.len(longest);

        for i in 1..(max + 1) {
            if i != longest {
                assert!(cs.len(i) <= length);
            }
        }
    }
}
