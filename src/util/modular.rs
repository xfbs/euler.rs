//! Modular arithmetic helpers.

/// Modular exponentiation.
pub fn exp_mod(number: u64, exponent: u64, modulo: u64) -> u64 {
    let mut product = 1;
    let mut current = number;
    let mut exponent = exponent;
    let mut i = 0;

    while exponent != 0 {
        if exponent & (1 << i) != 0 {
            product = mult_mod(product, current, modulo);
            exponent -= 1 << i;
        }

        i += 1;
        current = mult_mod(current, current, modulo);
        current %= modulo;
    }

    product
}

/// Modular multiplications, assumes both a and b are already mod modulo.
///
/// Will split numbers into upper and lower bits, multiply them separately.
pub fn mult_mod(a: u64, b: u64, modulo: u64) -> u64 {
    // split numbers into upper and lower bits: we can't multiply both numbers
    // without risking an overflow, so we gotta do it separately.
    let lower_a = a & u32::MAX as u64;
    let lower_b = b & u32::MAX as u64;
    let upper_a = (a >> 32) & u32::MAX as u64;
    let upper_b = (b >> 32) & u32::MAX as u64;

    let lower_lower = (lower_a * lower_b) % modulo;

    // well, if there are no upper bits, then we're done here.
    if upper_a == 0 && upper_b == 0 {
        return lower_lower;
    }

    let lower_upper = (lower_a * upper_b) % modulo;
    let upper_lower = (upper_a * lower_b) % modulo;
    let upper_upper = (upper_a * upper_b) % modulo;

    let lower_upper = upshift_mod(lower_upper, 32, modulo);
    let upper_lower = upshift_mod(upper_lower, 32, modulo);
    let upper_upper = upshift_mod(upper_upper, 64, modulo);

    (lower_lower + lower_upper + upper_lower + upper_upper) % modulo
}

/// Modular upshift. Calculates num << amount % modulo.
///
/// Assumes number is already mod modulo. Will not work if number has no
/// leading zeros.
pub fn upshift_mod(number: u64, amount: u64, modulo: u64) -> u64 {
    debug_assert!(number.leading_zeros() != 0);
    let mut number = number;
    let mut amount = amount;

    while amount != 0 {
        // upshifting a zero won't ever do anything, and it will actually cause
        // a panic because rust considers it to be an overflow.
        if number == 0 {
            return number;
        }

        let cur = (number.leading_zeros() as u64).min(amount);
        number <<= cur;
        number %= modulo;
        amount -= cur;
    }

    number
}

#[test]
fn test_mult_mod() {
    assert_eq!(mult_mod(1, 3, 10), 3);
    assert_eq!(mult_mod(2, 3, 10), 6);
    assert_eq!(mult_mod(3, 3, 10), 9);
    assert_eq!(mult_mod(4, 3, 10), 2);
    assert_eq!(mult_mod(5, 3, 10), 5);
    assert_eq!(mult_mod(6, 3, 10), 8);
    assert_eq!(
        mult_mod(10_u64.pow(10) - 1, 10_u64.pow(10) - 1, 10_u64.pow(10)),
        1
    );
    assert_eq!(
        mult_mod(
            3704982164889054723,
            2464556005047902547,
            6262212064215455187
        ),
        3711709673385903066
    );
    assert_eq!(
        mult_mod(349099074040092237, 469506628546504988, 1058267391949288817),
        548300163598111298
    );
    assert_eq!(
        mult_mod(2108579551800364029, 428889698667152279, 6274467423335728652),
        2812991774271529187
    );
    assert_eq!(
        mult_mod(269865024663446305, 1762745161874951867, 2026028848830680757),
        1286971789648142102
    );
    assert_eq!(
        mult_mod(
            5396225685828774292,
            5442688611907888877,
            6044460814425858166
        ),
        4090991302689775442
    );
    assert_eq!(
        mult_mod(
            2482591058681399621,
            2143456329514810747,
            5099034069133553110
        ),
        3538706862073889357
    );
}

#[test]
fn test_upshift_mod() {
    assert_eq!(upshift_mod(7, 100, 13), 8);
    assert_eq!(upshift_mod(3, 125, 17), 11);
    assert_eq!(upshift_mod(24213, 24314, 23840239), 21072149);
}

#[test]
fn test_exponent_mod() {
    assert_eq!(exp_mod(1, 0, 1000), 1);
    assert_eq!(exp_mod(91, 0, 1000), 1);
    assert_eq!(exp_mod(991, 0, 1000), 1);
    assert_eq!(exp_mod(1, 1, 1000), 1);
    assert_eq!(exp_mod(91, 1, 1000), 91);
    assert_eq!(exp_mod(991, 1, 1000), 991);
    assert_eq!(exp_mod(2, 2, 1000), 4);
    assert_eq!(exp_mod(2, 3, 1000), 8);
    assert_eq!(exp_mod(2, 4, 1000), 16);
    assert_eq!(exp_mod(2, 5, 1000), 32);
    assert_eq!(exp_mod(2, 6, 1000), 64);
    assert_eq!(exp_mod(2, 7, 1000), 128);
    assert_eq!(exp_mod(2, 10, 7), 2);
    assert_eq!(exp_mod(245322687, 37942, 23413527), 8989623);
    assert_eq!(exp_mod(2, 7830457, 10_u64.pow(10)), 9700303872);
}
