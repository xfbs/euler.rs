//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
//! Large non-Mersenne prime
//!
//! The way to go about this is to use modular arithmetic.
use crate::info::EulerInfo;
use crate::util::modular::*;

pub const INFO: EulerInfo = EulerInfo {
    number: 97,
    title: "Large non-Mersenne prime",
    slug: "large-non-mersenne-prime",
    solve: Some(solve_default),
    solution: Some("$2b$04$yx/UMNHGlhvSihqEUXg32eHeNPrTOTUnHncI2TXBWqLmpHv9V/EtS"),
};

pub fn solve_default() -> String {
    solve().to_string()
}

pub fn solve() -> u64 {
    let modulo = 10_u64.pow(10);
    (mult_mod(28433, exp_mod(2, 7830457, modulo), modulo) + 1) % modulo
}
