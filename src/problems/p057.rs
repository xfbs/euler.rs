//! # Problem 57: Square root convergents
//!
//! It is possible to show that the square root of two can be expressed as
//! an infinite continued fraction. √ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) =
//! 1.414213... By expanding this for the first four iterations, we get: 1 +
//! 1/2 = 3/2 = 1.5 1 + 1/(2 + 1/2) = 7/5 = 1.4 1 + 1/(2 + 1/(2 + 1/2)) =
//! 17/12 = 1.41666... 1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
//! The next three expansions are 99/70, 239/169, and 577/408, but the
//! eighth expansion, 1393/985, is the first example where the number of
//! digits in the numerator exceeds the number of digits in the denominator.
//! In the first one-thousand expansions, how many fractions contain a
//! numerator with more digits than denominator?
use crate::problems::EulerInfo;
use num::BigUint;

pub const INFO: EulerInfo = EulerInfo {
    number: 57,
    title: "Square root convergents",
    slug: "square-root-convergents",
    solve: Some(solve_default),
    solution: Some("$2a$10$LcfQNrorVdV91U4BE36Q0eThsgwqKbqyNnXG8actgM0hyeW.wpkFC"),
};

#[derive(Clone, Debug, Copy, PartialEq, Eq)]
pub struct Fraction<T> {
    numerator: T,
    denominator: T,
}

pub trait Width {
    fn width(&self) -> u64;
}

impl Width for BigUint {
    fn width(&self) -> u64 {
        self.to_str_radix(10).len() as u64
    }
}

impl Fraction<BigUint> {
    pub fn new(numerator: u64, denominator: u64) -> Self {
        Fraction {
            numerator: numerator.into(),
            denominator: denominator.into(),
        }
    }

    pub fn add(&mut self, other: &Self) {
        if self.denominator == other.denominator {
            self.numerator += &other.numerator;
            return;
        }

        let denominator = self.denominator.clone();

        // multiply fraction
        self.denominator *= &other.denominator;
        self.numerator *= &other.denominator;

        // add numerator
        self.numerator += &other.numerator * &denominator;
    }

    pub fn invert(&mut self) {
        let denominator = self.denominator.clone();
        self.denominator = self.numerator.clone();
        self.numerator = denominator;
    }
}

pub struct SquareRoot {
    current: Fraction<BigUint>,
}

impl SquareRoot {
    pub fn new() -> Self {
        SquareRoot {
            current: Fraction::<BigUint>::new(1, 1),
        }
    }
}

impl Iterator for SquareRoot {
    type Item = Fraction<BigUint>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current.add(&Fraction::<BigUint>::new(1, 1));
        self.current.invert();
        self.current.add(&Fraction::<BigUint>::new(1, 1));
        Some(self.current.clone())
    }
}

pub fn solve(max: usize) -> usize {
    SquareRoot::new()
        .take(max)
        .filter(|s| s.numerator.width() > s.denominator.width())
        .count()
}

pub fn solve_default() -> String {
    solve(1000).to_string()
}

#[test]
fn square_root_iter() {
    let mut iter = SquareRoot::new();
    assert_eq!(iter.next(), Some(Fraction::new(3, 2)));
    assert_eq!(iter.next(), Some(Fraction::new(7, 5)));
    assert_eq!(iter.next(), Some(Fraction::new(17, 12)));
    assert_eq!(iter.next(), Some(Fraction::new(41, 29)));
    assert_eq!(iter.next(), Some(Fraction::new(99, 70)));
    assert_eq!(iter.next(), Some(Fraction::new(239, 169)));
    assert_eq!(iter.next(), Some(Fraction::new(577, 408)));
    assert_eq!(iter.next(), Some(Fraction::new(1393, 985)));
}

#[test]
fn square_root_100() {
    let mut iter = SquareRoot::new();
    for _ in 0..100 {
        iter.next();
    }
}

#[test]
fn solve_small() {
    assert_eq!(solve(8), 1);
}
