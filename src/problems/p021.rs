//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::ToDivisors;

pub const INFO: EulerInfo = EulerInfo {
    number: 21,
    title: "Amicable numbers",
    slug: "amicable-numbers",
    solve: Some(solve_default),
    solution: Some("$2b$04$AsENw3gqZNC.bBt2qW3ClOfH2YfUsU6inBknB0bP1TiF9MU1vGXAy"),
};

pub fn solve_default() -> String {
    solve(10000).to_string()
}

pub fn solve(max: u32) -> u64 {
    (1..max).map(|n| amicable(n as u64)).sum()
}

pub fn amicable(num: u64) -> u64 {
    let partner: u64 = num.divisors().sum();

    if num != partner && num == partner.divisors().sum::<u64>() {
        partner
    } else {
        0
    }
}

#[test]
fn test_amicable() {
    assert_eq!(amicable(220), 284);
    assert_eq!(amicable(219), 0);
    assert_eq!(amicable(221), 0);
    assert_eq!(amicable(284), 220);
    assert_eq!(amicable(283), 0);
    assert_eq!(amicable(285), 0);
}

#[test]
fn test_solution() {
    assert_eq!(solve(285), 220 + 284);
}
