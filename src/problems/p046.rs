//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;

pub const INFO: EulerInfo = EulerInfo {
    number: 46,
    title: "Goldbach's other conjecture",
    slug: "goldbach's-other-conjecture",
    solve: Some(solve_default),
    solution: Some("$2b$04$Ig5jbyjK28qmm9RW8ICYdOQjb1OQmLpaxVmtS9qk3yl1Xk363XoMe"),
};

pub fn solve_default() -> String {
    solve().to_string()
}

pub fn solve() -> u64 {
    // FIXME: hack. since I can't have two mutable references to a prime (one
    // being primes.check() and the other one being primes.into_iter(), I just
    // resort to having two separate primes generators, which is of course not
    // exactly efficient, but works for now.
    let mut primes = Prime::new();
    let mut other = Prime::new();

    (3..)
        .filter(|&n| n % 2 == 1)
        .filter(|&n| !primes.check(n))
        .find(|&c| !conjecture(&mut other, c))
        .unwrap()
}

pub fn conjecture(primes: &mut Prime, odd_composite: u64) -> bool {
    primes
        .into_iter()
        .skip(1)
        .take_while(|&prime| prime < odd_composite)
        .map(|prime| (odd_composite - prime) / 2)
        .any(|prime| ((prime as f64).sqrt() as u64).pow(2) == prime)
}

#[test]
fn test_conjecture() {
    let mut primes = Prime::new();

    assert!(conjecture(&mut primes, 1005));
    assert!(!conjecture(&mut primes, 5993));
}

#[test]
fn test_solve() {
    let mut primes = Prime::new();

    assert!(!conjecture(&mut primes, solve()));
}
