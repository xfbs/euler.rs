use xfbs_euler::tool;

fn main() {
    match tool::main() {
        Ok(_) => {}
        Err(x) => {
            println!("error: {}", x);
            std::process::exit(1);
        }
    }
}
