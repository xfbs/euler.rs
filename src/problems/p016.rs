//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 16,
    title: "Power digit sum",
    slug: "power-digit-sum",
    solve: Some(solve_default),
    solution: Some("$2b$04$aO3Jou./FjdTEY8UPDoieO/gl/pvqBsH8OS3nOWhHgVJEEQm/vmLi"),
};

pub fn solve_default() -> String {
    solve(1000).to_string()
}

pub fn solve(exp: usize) -> u64 {
    let mut num: Vec<u8> = vec![1];

    for _ in 0..exp {
        let mut carry = 0;

        for d in num.iter_mut() {
            let cur = 2 * (*d as u64) + carry;
            *d = (cur % 10) as u8;
            carry = cur / 10;
        }

        if carry != 0 {
            num.push(carry as u8);
        }
    }

    num.iter().map(|n| *n as u64).sum()
}

#[test]
fn test_solve() {
    assert_eq!(solve(1), 2);
    assert_eq!(solve(2), 4);
    assert_eq!(solve(3), 8);
    assert_eq!(solve(4), 1 + 6);
    assert_eq!(solve(5), 3 + 2);
    assert_eq!(solve(6), 6 + 4);
    assert_eq!(solve(7), 1 + 2 + 8);
    assert_eq!(solve(8), 2 + 5 + 6);
    assert_eq!(solve(9), 5 + 1 + 2);
    assert_eq!(solve(10), 1 + 0 + 2 + 4);
    assert_eq!(solve(11), 2 + 0 + 4 + 8);
}
