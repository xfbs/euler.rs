//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use std::fmt;

pub const INFO: EulerInfo = EulerInfo {
    number: 96,
    title: "Su Doku",
    slug: "su-doku",
    solve: Some(solve_default),
    solution: Some("$2b$04$6opnfWMFMtySfI6s35ne0eS5co7dLlboLZfYnFJ791SdcUNjyHXV."),
};

pub const DATA: &'static str = include_str!("p096.txt");

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Cell {
    Solved(u8),
    Unsolved([bool; 9]),
}

impl Cell {
    fn solution(&self) -> Option<u8> {
        match self {
            Cell::Solved(x) => Some(*x),
            _ => None,
        }
    }

    fn fixup(&mut self) {
        match self {
            Cell::Solved(_) => {}
            Cell::Unsolved(candidates) => {
                if candidates.iter().filter(|x| **x).count() == 1 {
                    let (pos, _) = candidates
                        .iter()
                        .enumerate()
                        .find(|x| *x.1 == true)
                        .unwrap();
                    *self = Cell::Solved(pos as u8 + 1);
                }
            }
        }
    }
}

#[test]
fn test_cell_solution_works() {
    assert_eq!(Cell::Solved(1).solution(), Some(1));
    assert_eq!(Cell::Solved(2).solution(), Some(2));
    assert_eq!(Cell::Solved(3).solution(), Some(3));
    assert_eq!(Cell::Solved(4).solution(), Some(4));
    assert_eq!(Cell::Unsolved([false; 9]).solution(), None);
}

#[test]
fn test_cell_fixup_works() {
    let mut candidates = [false; 9];
    candidates[4] = true;
    let mut cell = Cell::Unsolved(candidates);
    cell.fixup();
    assert_eq!(cell, Cell::Solved(5));
}

#[derive(Copy, Clone)]
pub struct Sudoku {
    cells: [[Cell; 9]; 9],
}

#[derive(Copy, Clone)]
pub struct SudokuRow<'a> {
    sudoku: &'a Sudoku,
    row: usize,
    col: usize,
}

#[derive(Copy, Clone)]
pub struct SudokuCol<'a> {
    sudoku: &'a Sudoku,
    row: usize,
    col: usize,
}

#[derive(Copy, Clone)]
pub struct SudokuSquare<'a> {
    sudoku: &'a Sudoku,
    num: usize,
    pos: usize,
}

impl<'a> Iterator for SudokuRow<'a> {
    type Item = &'a Cell;

    fn next(&mut self) -> Option<Self::Item> {
        if self.col == 9 {
            None
        } else {
            self.col += 1;
            Some(&self.sudoku.cells[self.row][self.col - 1])
        }
    }
}

impl<'a> Iterator for SudokuCol<'a> {
    type Item = &'a Cell;

    fn next(&mut self) -> Option<Self::Item> {
        if self.row == 9 {
            None
        } else {
            self.row += 1;
            Some(&self.sudoku.cells[self.row - 1][self.col])
        }
    }
}

impl<'a> Iterator for SudokuSquare<'a> {
    type Item = &'a Cell;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos == 9 {
            None
        } else {
            let base_row = 3 * (self.num / 3);
            let base_col = 3 * (self.num % 3);

            let cur_row = self.pos / 3;
            let cur_col = self.pos % 3;

            let row = base_row + cur_row;
            let col = base_col + cur_col;

            self.pos += 1;
            Some(&self.sudoku.cells[row][col])
        }
    }
}

impl Sudoku {
    fn new() -> Sudoku {
        Sudoku {
            cells: [[Cell::Unsolved([true; 9]); 9]; 9],
        }
    }

    fn from_lines(lines: &[&str]) -> Sudoku {
        let mut sudoku = Sudoku::new();

        for (line_num, line) in lines.iter().enumerate() {
            for (col_num, item) in line.chars().enumerate() {
                let digit = item.to_digit(10).unwrap();

                if 1 <= digit && digit <= 9 {
                    sudoku.cells[line_num][col_num] = Cell::Solved(digit as u8);
                }
            }
        }

        sudoku
    }

    fn is_solved(&self) -> bool {
        self.cells.iter().all(|row| {
            row.iter().all(|item| match item {
                Cell::Solved(_) => true,
                _ => false,
            })
        })
    }

    fn solve(&mut self) -> bool {
        if self.solve_eliminate() {
            true
        } else {
            self.solve_guess()
        }
    }

    fn solve_eliminate(&mut self) -> bool {
        loop {
            let mut changed = false;
            for r in 0..9 {
                for c in 0..9 {
                    let mut item = self.cells[r][c];
                    match item {
                        Cell::Solved(_) => {}
                        Cell::Unsolved(ref mut x) => {
                            let rows = self.row(r);
                            let cols = self.column(c);
                            let square = self.square(r, c);

                            for cell in rows.chain(cols).chain(square) {
                                match cell {
                                    Cell::Solved(n) => {
                                        x[*n as usize - 1] = false;
                                    }
                                    Cell::Unsolved(_) => {}
                                }
                            }
                        }
                    }

                    // turn a cell with a single solution into a solved cell
                    item.fixup();

                    if self.cells[r][c] != item {
                        self.cells[r][c] = item;
                        changed = true;
                    }
                }
            }

            if !changed {
                break;
            }
        }

        self.is_solved()
    }

    fn solve_guess(&mut self) -> bool {
        // find place with least amout of choices
        let (row, col, _len) = self
            .cells
            .iter()
            .enumerate()
            .map(|(r, row)| row.iter().enumerate().map(move |(c, item)| (r, c, item)))
            .flatten()
            .map(|(r, c, item)| match item {
                Cell::Solved(_) => (r, c, 9),
                Cell::Unsolved(x) => (r, c, x.iter().filter(|b| **b).count()),
            })
            .min_by(|(_, _, a), (_, _, b)| a.cmp(b))
            .unwrap();

        let item = self.cells[row][col];

        if let Cell::Unsolved(choices) = item {
            for (pos, choice) in choices.iter().enumerate() {
                if *choice {
                    let backup = *self;
                    self.cells[row][col] = Cell::Solved(pos as u8 + 1);
                    self.solve();

                    if self.is_solved() {
                        return true;
                    } else {
                        *self = backup;
                    }
                }
            }
        } else {
            panic!("oops!");
        }

        self.is_solved()
    }

    fn row(&self, number: usize) -> SudokuRow {
        SudokuRow {
            sudoku: &self,
            row: number,
            col: 0,
        }
    }

    fn column(&self, number: usize) -> SudokuCol {
        SudokuCol {
            sudoku: &self,
            row: 0,
            col: number,
        }
    }

    fn square(&self, row: usize, col: usize) -> SudokuSquare {
        let base_row = row / 3;
        let base_col = col / 3;
        let num = 3 * base_row + base_col;

        SudokuSquare {
            sudoku: &self,
            num: num,
            pos: 0,
        }
    }
}

#[test]
fn test_sudoku_row_iter() {
    let mut sudoku = Sudoku::new();
    for i in 0..9 {
        sudoku.cells[0][i] = Cell::Solved(i as u8 + 1);
        sudoku.cells[4][i] = Cell::Solved(i as u8 + 1);
    }

    let row = sudoku.row(0).map(|c| *c).collect::<Vec<Cell>>();
    let row_expected = (1..=9)
        .map(|c| Cell::Solved(c as u8))
        .collect::<Vec<Cell>>();
    assert_eq!(row, row_expected);

    let row = sudoku.row(1).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(row, row_expected);

    let row = sudoku.row(4).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(row, row_expected);
}

#[test]
fn test_sudoku_col_iter() {
    let mut sudoku = Sudoku::new();
    for i in 0..9 {
        sudoku.cells[i][0] = Cell::Solved(i as u8 + 1);
    }

    let col = sudoku.column(0).map(|c| *c).collect::<Vec<Cell>>();
    let col_expected = (1..=9)
        .map(|c| Cell::Solved(c as u8))
        .collect::<Vec<Cell>>();
    assert_eq!(col, col_expected);

    let col = sudoku.column(1).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(col, col_expected);
}

#[test]
fn test_sudoku_square_iter() {
    let mut sudoku = Sudoku::new();
    sudoku.cells[0][0] = Cell::Solved(1);
    sudoku.cells[0][1] = Cell::Solved(2);
    sudoku.cells[0][2] = Cell::Solved(3);
    sudoku.cells[1][0] = Cell::Solved(4);
    sudoku.cells[1][1] = Cell::Solved(5);
    sudoku.cells[1][2] = Cell::Solved(6);
    sudoku.cells[2][0] = Cell::Solved(7);
    sudoku.cells[2][1] = Cell::Solved(8);
    sudoku.cells[2][2] = Cell::Solved(9);

    let expected = (1..=9)
        .map(|c| Cell::Solved(c as u8))
        .collect::<Vec<Cell>>();
    let square = sudoku.square(0, 0).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(0, 1).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(0, 2).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(1, 0).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(1, 1).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(1, 2).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(2, 0).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(2, 1).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(2, 2).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);

    let square = sudoku.square(0, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);
    let square = sudoku.square(1, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);
    let square = sudoku.square(2, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);
    let square = sudoku.square(3, 0).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);
    let square = sudoku.square(3, 1).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);
    let square = sudoku.square(3, 2).map(|c| *c).collect::<Vec<Cell>>();
    assert_ne!(square, expected);

    sudoku.cells[3][3] = Cell::Solved(1);
    sudoku.cells[3][4] = Cell::Solved(2);
    sudoku.cells[3][5] = Cell::Solved(3);
    sudoku.cells[4][3] = Cell::Solved(4);
    sudoku.cells[4][4] = Cell::Solved(5);
    sudoku.cells[4][5] = Cell::Solved(6);
    sudoku.cells[5][3] = Cell::Solved(7);
    sudoku.cells[5][4] = Cell::Solved(8);
    sudoku.cells[5][5] = Cell::Solved(9);

    let square = sudoku.square(3, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(3, 4).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(3, 5).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(4, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(4, 4).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(4, 5).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(5, 3).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(5, 4).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
    let square = sudoku.square(5, 5).map(|c| *c).collect::<Vec<Cell>>();
    assert_eq!(square, expected);
}

#[test]
fn test_sudoku_solve_eliminate() {
    let mut sudoku = Sudoku::new();
    for i in 0..8 {
        sudoku.cells[0][i] = Cell::Solved(i as u8 + 1);
    }

    sudoku.solve_eliminate();
    assert_eq!(sudoku.cells[0][8], Cell::Solved(9));

    sudoku.cells[2][0] = Cell::Solved(4);

    sudoku.solve_eliminate();
    assert_eq!(
        sudoku.cells[1][0],
        Cell::Unsolved([false, false, false, false, true, true, true, true, true])
    );

    sudoku.cells[3][0] = Cell::Solved(2);
    sudoku.cells[4][0] = Cell::Solved(3);

    sudoku.solve_eliminate();
    assert_eq!(
        sudoku.cells[1][0],
        Cell::Unsolved([false, false, false, false, true, true, true, true, true])
    );

    for i in 5..9 {
        sudoku.cells[i][0] = Cell::Solved(i as u8 + 1);
    }

    sudoku.solve_eliminate();
    assert_eq!(sudoku.cells[1][0], Cell::Solved(5));

    sudoku = Sudoku::new();
    sudoku.cells[3][3] = Cell::Solved(3);
    sudoku.cells[3][4] = Cell::Solved(1);
    sudoku.cells[3][5] = Cell::Solved(4);
    sudoku.cells[4][3] = Cell::Solved(2);
    sudoku.cells[4][4] = Cell::Solved(6);
    sudoku.cells[4][5] = Cell::Solved(8);
    sudoku.cells[5][3] = Cell::Solved(9);
    sudoku.cells[5][5] = Cell::Solved(7);

    sudoku.solve_eliminate();
    assert_eq!(sudoku.cells[5][4], Cell::Solved(5));
}

impl fmt::Display for &Sudoku {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in 0..9 {
            for col in 0..9 {
                match self.cells[row][col] {
                    Cell::Solved(x) => write!(f, "{}", x)?,
                    Cell::Unsolved(_) => write!(f, " ")?,
                }
            }

            write!(f, "\n")?;
        }

        fmt::Result::Ok(())
    }
}

fn solve_default() -> String {
    DATA.lines()
        .collect::<Vec<&'static str>>()
        .chunks(10)
        .map(|w| Sudoku::from_lines(&w[1..]))
        .map(|s| {
            let mut sudoku = s.clone();
            sudoku.solve();
            sudoku
        })
        .map(|sudoku| {
            sudoku.cells[0]
                .iter()
                .take(3)
                .map(|c| c.solution().unwrap_or(0))
                .fold(0 as u16, |a, e| 10 * a + e as u16)
        })
        .fold(0, |a, e| a + e)
        .to_string()
}
