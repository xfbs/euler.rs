//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 42,
    title: "Coded triangle numbers",
    slug: "coded-triangle-numbers",
    solve: Some(solve_default),
    solution: Some("$2b$04$CZ0/WiGruBpczVscf0nKz.GrsGwDrjtjNpDD8OpSQHFbXGkAaAdry"),
};

pub const DATA: &'static str = include_str!("p042.txt");

pub fn solve_default() -> String {
    let mut words = DATA
        .split(',')
        .map(|word| {
            let len = word.len();
            word[1..len - 1].to_string()
        })
        .collect::<Vec<String>>();

    solve(&mut words).to_string()
}

pub fn solve(words: &mut Vec<String>) -> usize {
    words
        .iter()
        .map(|word| worth(word))
        .filter(|worth| is_triangular_number(*worth as u64))
        .count()
}

pub fn worth(s: &str) -> usize {
    s.chars().map(|c| c as usize - 'A' as usize + 1).sum()
}

fn is_triangular_number(y: u64) -> bool {
    let x = (2.0 * y as f64).sqrt() as u64;
    y == (x * (x + 1) / 2)
}

#[test]
fn test_solve() {
    assert_eq!(solve(&mut vec!["A".to_string()]), 1);
    assert_eq!(
        solve(&mut vec![
            "A".to_string(),
            "Z".to_string(),
            "SKY".to_string(),
        ]),
        2
    );
}
