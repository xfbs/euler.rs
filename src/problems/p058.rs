//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;
use crate::util::SpiralDiagonals;

pub const INFO: EulerInfo = EulerInfo {
    number: 58,
    title: "Spiral primes",
    slug: "spiral-primes",
    solve: Some(solve_default),
    solution: Some("$2b$04$uWdfgPDGcandgV/XN0Dc/.bypJlh3O9Hb3ZxkJQoSibs9KorCtwkC"),
};

pub fn solve_default() -> String {
    solve(10).to_string()
}

pub struct SpiralDiagonalsRatio {
    spiral_diagonals: SpiralDiagonals<u32>,
    primes: Prime,
    positive: usize,
    negative: usize,
}

impl SpiralDiagonalsRatio {
    pub fn new() -> SpiralDiagonalsRatio {
        SpiralDiagonalsRatio {
            spiral_diagonals: SpiralDiagonals::new(),
            primes: Prime::new(),
            positive: 0,
            negative: 0,
        }
    }

    pub fn next(&mut self) -> (usize, usize, usize) {
        let side_length = self.spiral_diagonals.side_length();

        loop {
            let cur = self.spiral_diagonals.get();

            if self.primes.check(cur as u64) {
                self.positive += 1;
            } else {
                self.negative += 1;
            }

            self.spiral_diagonals.next();

            if self.spiral_diagonals.is_beginning() {
                break;
            }
        }

        (side_length as usize, self.positive, self.negative)
    }
}

pub fn solve(percent: u8) -> usize {
    let mut spd = SpiralDiagonalsRatio::new();
    spd.next();
    spd.next();
    spd.next();

    loop {
        let (side_len, pos, neg) = spd.next();

        if (pos * 100) / (pos + neg) < (percent as usize) {
            return side_len;
        }
    }
}

#[test]
fn test_solve() {
    assert_eq!(solve(65), 7);
}

#[test]
fn test_spiral_diagonals_side_lengths() {
    let mut sd = SpiralDiagonals::new();
    let sds: Vec<u32> = (0..13)
        .map(|_| {
            let ret = sd.side_length();
            sd.next();
            ret
        })
        .collect();
    let res = vec![1, 3, 3, 3, 3, 5, 5, 5, 5, 7, 7, 7, 7];
    assert_eq!(sds, res);
}

#[test]
fn test_spiral_diagonals() {
    let mut sd = SpiralDiagonals::new();
    let sds: Vec<u32> = (0..13)
        .map(|_| {
            let ret = sd.get();
            sd.next();
            ret
        })
        .collect();
    let res = vec![1, 3, 5, 7, 9, 13, 17, 21, 25, 31, 37, 43, 49];
    assert_eq!(sds, res);
}

#[test]
fn test_spiral_diagonals_iterator() {
    let sds: Vec<u32> = SpiralDiagonals::new().take(13).collect();
    let res = vec![1, 3, 5, 7, 9, 13, 17, 21, 25, 31, 37, 43, 49];
    assert_eq!(sds, res);
}

#[test]
fn test_spiral_diagonals_ratio() {
    let mut spd = SpiralDiagonalsRatio::new();
    assert_eq!(spd.next(), (1, 0, 1));
    assert_eq!(spd.next(), (3, 3, 2));
    assert_eq!(spd.next(), (5, 5, 4));
    assert_eq!(spd.next(), (7, 8, 5));
}
