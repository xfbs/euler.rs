//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 99,
    title: "Largest exponential",
    slug: "largest-exponential",
    solve: Some(solve_default),
    solution: Some("$2a$10$NgbOch.hyS4z2hSZQjbFWeHhnodSUqKNFZzey9Ms1YK4FaTJhgjZG"),
};

pub const DATA: &'static str = include_str!("p099.txt");

pub fn solve_default() -> String {
    solve().to_string()
}

pub fn solve() -> usize {
    DATA.split("\n")
        .map(|n| {
            let mut numbers = n.split(",").map(|x| x.parse::<u64>().unwrap());
            (numbers.next().unwrap(), numbers.next().unwrap())
        })
        .enumerate()
        .map(|(line, (base, exponent))| (line, magnitude(base, exponent)))
        .max_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Less))
        .unwrap()
        .0
        + 1
}

pub fn magnitude(base: u64, exponent: u64) -> f64 {
    (exponent as f64) * (base as f64).log(10.0)
}

#[test]
fn test_magnitude() {
    assert_eq!(magnitude(10, 10) as u32, 10);
    assert_eq!(magnitude(142, 12) as u32, 25);
    assert_eq!(magnitude(1293812, 34324) as u32, 209783);
}
