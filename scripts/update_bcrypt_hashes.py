#!/usr/bin/env python3
# TODO: script to update bcrypt hashes

import sys
import json

def update_hashes(solve_results, check_results, src_dir):
    enabled = {}

    for check_result in check_results:
        enabled[check_result["number"]] = check_result["result"]

    for solve_result in solve_results:
        number = solve_result["number"]
        if enabled[number]:
            print("updating {}".format(number))
            file = open("src/problems/p{}/solution.bcrypt".format(str(number).rjust(3, '0')), mode='w')
            file.write(solve_result["output"])

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''\
    Update solution hashes from check_results.json and solve_results.json.
    ''',
    epilog='''
    The check_results.json and solve_results.json can be generated like this, respectively.

        xfbs-euler check -a --results check_results.json
        xfbs-euler solve -a --bycrypt --results solve_results.json
    ''')
    parser.add_argument('--solve', metavar='FILE', dest='solve_results', help="Path to solve_results.json.", required=True)
    parser.add_argument('--check', metavar='FILE', dest='check_results', help="Path to check_results.json.", required=True)
    parser.add_argument('--srcdir', metavar='PATH', dest='src_dir', help="Path to source code of euler.rs.", default=".")

    args = parser.parse_args()

    solve_results = json.load(open(args.solve_results))
    check_results = json.load(open(args.check_results))

    update_hashes(solve_results, check_results, args.src_dir)
