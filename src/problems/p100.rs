//! # Problem 100: Arranged probability
//!
//! If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs,
//! and two discs were taken at random, it can be seen that the probability of taking two blue
//! discs, P(BB) = (15/21)×(14/20) = 1/2. The next such arrangement, for which there is exactly 50%
//! chance of taking two blue discs at random, is a box containing eighty-five blue discs and
//! thirty-five red discs. By finding the first arrangement to contain over 1012 =
//! 1,000,000,000,000 discs in total, determine the number of blue discs that the box would
//! contain.
use num::integer::Roots;
use std::convert::TryInto;
use crate::info::EulerInfo;
use rayon::prelude::*;

pub const INFO: EulerInfo = EulerInfo {
    number: 100,
    title: "Arranged probability",
    slug: "arranged-probability",
    solve: Some(solve_default),
    solution: Some("$2a$10$y8z2QLhbUUovd4tc63414e.H2czfnQsdvsUYAuCJ9sJoTyLVtKkOu"),
};

/// Check that a number of blue and red disks fulfils the property that there is a 50% chance of
/// getting two blue disks after two draws.
pub fn check(b: u128, r: u128) -> bool {
    let b: i128 = b.try_into().unwrap();
    let r: i128 = r.try_into().unwrap();
    -b*b + 2*b*r + b + r*r - r == 0
}

#[test]
fn can_check() {
    assert!(check(15, 6));
    assert!(check(85, 35));
}

/// For a given count of blue disks, return how many red disks are needed to fulfil the propery of
/// [check()][]. Due to rounding, this result might not always be an exact solution.
pub fn red(b: u128) -> u128 {
    let b: i128 = b.try_into().unwrap();
    let result = ((8 * b * b - 8 * b + 1).sqrt() - 2 * b + 1) / 2;
    result.try_into().unwrap()
}

#[test]
fn can_red() {
    assert_eq!(red(0), 1);
    assert_eq!(red(10), 3);
    assert_eq!(red(15), 6);
    assert_eq!(red(20), 8);
    assert_eq!(red(85), 35);
}

/// For a given sum, return the minimum blue and red disk counts that have the given sum. The
/// solution might not be correct, so subsequent solutions must be checked.
pub fn min(sum: u128) -> (u128, u128) {
    let mut guess = (sum, red(sum));
    loop {
        let current_sum = guess.0 + guess.1;
        let prev_sum = (guess.0 - 1) + red(guess.0 - 1);
        if current_sum >= sum && prev_sum < sum {
            return guess;
        }
        if prev_sum >= sum {
            let blue = guess.0 - (prev_sum - sum).max(1);
            guess = (blue, red(blue));
        } else {
            let blue = guess.0 + (sum - current_sum);
            guess = (blue, red(blue));
        }
    }
}

#[test]
fn can_min() {
    assert_eq!(min(21), (15, 6));
    assert_eq!(min(100), (71, 29));
    assert_eq!(min(120), (85, 35));
}

#[test]
fn can_min_loop() {
    for val in [10, 100, 1000, 10_000, 100_000, 1_000_000, 10_000_000, 100_000_000, 1_000_000_000] {
        let (b, r) = min(val);
        assert!((b + r) >= val);
        assert!((b- 1 + red(b - 1)) < val);
    }
}

/// Solve
pub fn solve(sum: u128) -> (u128, u128) {
    let (start, _) = min(sum);
    (start..10*start).into_par_iter().map(|b| (b, red(b))).find(|(b, r)| check(*b, *r)).unwrap()
}

#[test]
fn can_solve() {
    assert_eq!(solve(10), (15, 6));
    assert_eq!(solve(100), (85, 35));
    assert_eq!(solve(1000), (2871, 1189));
    assert_eq!(solve(10000), (16731, 6930));
}

pub fn solve_default() -> String {
    solve(1_000_000_000_000).0.to_string()
}
