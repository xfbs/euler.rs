//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Lcm;

pub const INFO: EulerInfo = EulerInfo {
    number: 5,
    title: "Smallest multiple",
    slug: "smallest-multiple",
    solve: Some(solve_default),
    solution: Some("$2b$04$zcJAnOMy8ifVXb.4G5/slevl7PVMYz1bCHg0Xu5YHuiozA7UKShmW"),
};

pub fn solve_default() -> String {
    solve(20).to_string()
}

pub fn solve(max: u32) -> u32 {
    (1..(max + 1)).fold(1, |a, b| a.lcm(b))
}

#[test]
fn test_solve() {
    assert!(solve(2) == 2);
    assert!(solve(3) == 6);
    assert!(solve(4) == 12);
    assert!(solve(5) == 60);
    assert!(solve(6) == 60);
}
