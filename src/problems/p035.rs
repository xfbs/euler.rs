//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;
use crate::util::ToRotations;
use std::collections::HashSet;

pub const INFO: EulerInfo = EulerInfo {
    number: 35,
    title: "Circular primes",
    slug: "circular-primes",
    solve: Some(solve_default),
    solution: Some("$2b$04$addAuR3cnFM5KSeEF2/c0eAMaG3D6LaWs5iR8xw66B8ElZvOgitLm"),
};

pub fn solve_default() -> String {
    solve(1000000).to_string()
}

pub fn solve(max: u64) -> usize {
    let primes = Prime::new()
        .into_iter()
        .take_while(|n| *n < max)
        .collect::<Vec<u64>>();

    let mut rotatable = HashSet::new();

    for prime in &primes {
        if prime
            .rotations()
            .all(|rot| primes.binary_search(&rot).is_ok())
        {
            for rot in prime.rotations() {
                rotatable.insert(rot);
            }
        }
    }

    rotatable.iter().count()
}

#[test]
fn test_solve() {
    assert_eq!(solve(100), 13);
}
