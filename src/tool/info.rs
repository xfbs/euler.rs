use crate::problems;
use crate::tool::Subcommand;
use crate::tool::TestChoice;
use clap::{App, AppSettings, Arg, ArgMatches};

#[derive(Copy, Clone, Debug)]
pub struct Info {
    test: TestChoice,
}

impl Subcommand for Info {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("info")
            .about("Shows information about euler problems")
            .setting(AppSettings::ArgRequiredElseHelp)
            .arg(
                Arg::with_name("all")
                    .help("Show information on all problems")
                    .long("all")
                    .short("a"),
            )
            .arg(
                Arg::with_name("number")
                    .help("the problem to inspect")
                    .index(1),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        Info {
            test: parse_test_choice(matches),
        }
    }

    fn run(&self) -> Result<(), String> {
        match self.test {
            TestChoice::All => info_all(),
            TestChoice::Test(x) => info_number(x),
        }

        Ok(())
    }
}

fn info_all() {
    for x in problems::PROBLEMS.iter() {
        println!("{}", x);
    }
}

fn info_number(number: usize) {
    match problems::PROBLEMS.get(number - 1) {
        Some(x) => println!("{}", x),
        None => println!("error: not found"),
    }
}

fn parse_test_choice(a: &ArgMatches) -> TestChoice {
    if a.is_present("all") {
        TestChoice::All
    } else {
        let number = a
            .value_of("number")
            .and_then(|x| x.parse::<usize>().ok())
            .unwrap();
        TestChoice::Test(number)
    }
}
