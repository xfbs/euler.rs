//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use std::cmp;

pub const INFO: EulerInfo = EulerInfo {
    number: 9,
    title: "Special pythagoean triplet",
    slug: "special-pythagoran-triplet",
    solve: Some(solve_default),
    solution: Some("$2b$04$PRYmtYD47/bdi4Y7MFnAwu2L9yKAAqHrwCY8EyJ.jMbj2nHDfUgCW"),
};

pub fn solve_default() -> String {
    solve(1000).to_string()
}

pub fn solve(n: u32) -> u32 {
    let mut triplet: (u32, u32, u32) = (0, 0, 0);

    // choose c so that it's not bigger than n
    for c in 1..n {
        // choose b so that it's no bigger than
        // c and so that c+b are smaller than n
        for b in 1..cmp::min(c, n - c) {
            // choose a so that a+b+c=n
            let a = n - b - c;

            // check if it's a pythagorean
            // triplet, and if so, save it
            if a.pow(2) + b.pow(2) == c.pow(2) {
                triplet = (a, b, c);
            }
        }
    }

    // return product
    triplet.0 * triplet.1 * triplet.2
}

#[test]
fn test_solve() {
    assert!(solve(3 + 4 + 5) == 3 * 4 * 5);
}
