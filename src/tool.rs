use clap::{App, AppSettings, ArgMatches};

mod check;
mod info;
mod solve;

use check::Check;
use info::Info;
use solve::Solve;

pub fn app<'a, 'b>() -> App<'a, 'b> {
    App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .setting(AppSettings::ArgRequiredElseHelp)
        .subcommand(Info::app())
        .subcommand(Check::app())
        .subcommand(Solve::app())
}

pub fn main() -> Result<(), String> {
    let matches = app().get_matches();

    match matches.subcommand() {
        ("info", Some(ref x)) => Info::parse(x).run().unwrap(),
        ("check", Some(ref x)) => Check::parse(x).run().unwrap(),
        ("solve", Some(ref x)) => Solve::parse(x).run().unwrap(),
        _ => {}
    }

    Ok(())
}

pub trait Subcommand {
    fn app<'a, 'b>() -> App<'a, 'b>;
    fn parse(matches: &ArgMatches) -> Self;
    fn run(&self) -> Result<(), String>;
}

#[derive(Copy, Clone, Debug)]
pub enum TestChoice {
    All,
    Test(usize),
}

#[derive(Copy, Clone, Debug)]
pub enum HashChoice {
    None,
    BCrypt(u32),
}
