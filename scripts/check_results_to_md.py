#!/usr/bin/env python3

import json
import sys
from datetime import datetime

def time_to_ms(time):
    return str(round(10**3 * time["secs"] + time["nanos"] / 10.0**6, 2))

def results_to_md(json):
    print("# Progress")
    print("")
    print("This is a table consisting of the current progress.")
    print("")
    print("| Number | Title | Solved (time) | Links |")
    print("| ------ | ----- | ------------- | ----- |")
    for problem in json:
        solved = "No"
        if problem["result"]:
            solved="Yes ({} ms)".format(time_to_ms(problem["time"]))
        pnum = "p" + str(problem["number"]).rjust(3, '0')
        print("| {} | {} | {} | [src](https://gitlab.com/xfbs/euler.rs/-/blob/master/src/problems/{}/mod.rs) [doc](https://xfbs.gitlab.io/euler.rs/docs/xfbs_euler/problems/{}/index.html) [desc](https://gitlab.com/xfbs/euler.rs/-/blob/master/src/problems/{}/problem.md) |".format(problem["number"], problem["name"], solved, pnum, pnum, pnum))
    print("")
    print("This table was generated on {} UTC and is updated on every commit to the `master` branch.".format(datetime.utcnow().isoformat()))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Usage: {} results.json".format(sys.argv[0]))
        print("")
        print("Parses results JSON file resulting from xfbs-euler check, and generates a")
        print("markdown document containing the results.")
    else:
        results_to_md(json.load(open(sys.argv[1])))


