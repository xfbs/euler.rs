use crate::info::EulerInfo;

pub mod p001;
pub mod p002;
pub mod p003;
pub mod p004;
pub mod p005;
pub mod p006;
pub mod p007;
pub mod p008;
pub mod p009;
pub mod p010;
pub mod p011;
pub mod p012;
pub mod p013;
pub mod p014;
pub mod p015;
pub mod p016;
pub mod p017;
pub mod p018;
pub mod p019;
pub mod p020;
pub mod p021;
pub mod p022;
pub mod p023;
pub mod p024;
pub mod p025;
pub mod p026;
pub mod p027;
pub mod p028;
pub mod p029;
pub mod p030;
pub mod p031;
pub mod p032;
pub mod p033;
pub mod p034;
pub mod p035;
pub mod p036;
pub mod p037;
pub mod p038;
pub mod p039;
pub mod p040;
pub mod p041;
pub mod p042;
pub mod p043;
pub mod p044;
pub mod p045;
pub mod p046;
pub mod p047;
pub mod p048;
pub mod p049;
pub mod p050;
pub mod p052;
pub mod p053;
pub mod p055;
pub mod p057;
pub mod p058;
//pub mod p059;
pub mod p067;
pub mod p081;
pub mod p082;
pub mod p083;
pub mod p092;
pub mod p096;
pub mod p097;
pub mod p099;
pub mod p100;

pub const PROBLEMS: &[&'static EulerInfo] = &[
    &p001::INFO,
    &p002::INFO,
    &p003::INFO,
    &p004::INFO,
    &p005::INFO,
    &p006::INFO,
    &p007::INFO,
    &p008::INFO,
    &p009::INFO,
    &p010::INFO,
    &p011::INFO,
    &p012::INFO,
    &p013::INFO,
    &p014::INFO,
    &p015::INFO,
    &p016::INFO,
    &p017::INFO,
    &p018::INFO,
    &p019::INFO,
    &p020::INFO,
    &p021::INFO,
    &p022::INFO,
    &p023::INFO,
    &p024::INFO,
    &p025::INFO,
    &p026::INFO,
    &p027::INFO,
    &p028::INFO,
    &p029::INFO,
    &p030::INFO,
    &p031::INFO,
    &p032::INFO,
    &p033::INFO,
    &p034::INFO,
    &p035::INFO,
    &p036::INFO,
    &p037::INFO,
    &p038::INFO,
    &p039::INFO,
    &p040::INFO,
    &p041::INFO,
    &p042::INFO,
    &p043::INFO,
    &p044::INFO,
    &p045::INFO,
    &p046::INFO,
    &p047::INFO,
    &p048::INFO,
    &p049::INFO,
    &p050::INFO,
    &p052::INFO,
    &p053::INFO,
    &p055::INFO,
    &p057::INFO,
    &p058::INFO,
    &p067::INFO,
    &p081::INFO,
    &p082::INFO,
    &p083::INFO,
    &p092::INFO,
    &p096::INFO,
    &p097::INFO,
    &p099::INFO,
    &p100::INFO,
];

#[test]
fn test_problems_ordered() {
    assert!(PROBLEMS.windows(2).all(|w| w[0].number < w[1].number));
}
