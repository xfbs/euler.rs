//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 82,
    title: "Path sum: three ways",
    slug: "path-sum:-three-ways",
    solve: Some(solve_default),
    solution: Some("$2b$04$dEEOSVQcwknOKK2vUJwO2uywQw4EtTk.ZOoF.vL/MyZ7aGSvtKxgq"),
};

pub const DATA: &'static str = include_str!("p082.txt");

pub fn solve_default() -> String {
    let data = DATA
        .lines()
        .map(|line| {
            line.split(",")
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    path_sum_three_ways(&data).to_string()
}

pub fn path_sum_three_ways(data: &Vec<Vec<u32>>) -> u32 {
    let rows = data.len();
    let cols = data.get(0).map(|n| n.len()).unwrap_or(0);

    // get a list of columns
    let transposed = (0..cols)
        .map(|col| (0..rows).map(|row| data[row][col]).collect::<Vec<u32>>())
        .collect::<Vec<Vec<u32>>>();

    // this is the only one we mess with.
    let mut paths = transposed[0].clone();

    for column in transposed.iter().skip(1) {
        let new_paths = (0..paths.len())
            .map(|row_num| {
                (0..paths.len())
                    .map(|origin| {
                        if row_num < origin {
                            column[row_num..origin + 1].iter().sum::<u32>() + paths[origin]
                        } else {
                            column[origin..row_num + 1].iter().sum::<u32>() + paths[origin]
                        }
                    })
                    .min()
                    .unwrap()
            })
            .collect();
        paths = new_paths;
    }

    *paths.iter().min().unwrap()
}

#[test]
fn test_path_sum_three_ways_one_col() {
    assert_eq!(path_sum_three_ways(&vec![vec![4]]), 4);
    assert_eq!(path_sum_three_ways(&vec![vec![4, 2]]), 6);
    assert_eq!(path_sum_three_ways(&vec![vec![4], vec![2]]), 2);

    assert_eq!(path_sum_three_ways(&vec![vec![4, 2], vec![2, 5]]), 6);
    assert_eq!(path_sum_three_ways(&vec![vec![5, 2], vec![2, 4]]), 6);

    assert_eq!(path_sum_three_ways(&vec![vec![1, 1, 9], vec![9, 1, 1]]), 4);
    assert_eq!(path_sum_three_ways(&vec![vec![9, 1, 1], vec![1, 1, 9]]), 4);
}
