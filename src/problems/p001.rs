//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Lcm;

pub const INFO: EulerInfo = EulerInfo {
    number: 1,
    title: "Multiples of 3 and 5",
    slug: "multiples-of-3-and-5",
    solve: Some(solve_default),
    solution: Some("$2b$04$wD8R4YpmBVMuoZN6M07Fv.Vsoz3a.vp9vTjSelqGfI7qSURIIISDG"),
};

pub fn solve_default() -> String {
    solve(999, (3, 5)).to_string()
}

// find the sum of all numbers between 0 and max that are divisible
// by any of the two divisors
pub fn solve(max: u32, divisors: (u32, u32)) -> u32 {
    let (a, b) = divisors;

    sum(max, a) + sum(max, b) - sum(max, a.lcm(b))
}

// find the sum of all numbers that are divisible by the divisor
fn sum(max: u32, divisor: u32) -> u32 {
    // find the biggest number <= max that is divisible by divisor
    let max = max - (max % divisor);

    (max * ((max / divisor) + 1)) / 2
}

#[test]
fn sum_works() {
    assert!(sum(10, 3) == 18);
    assert!(sum(10, 5) == 15);
    assert!(sum(22, 7) == 42);
    assert!(sum(43, 51) == 0);
    assert!(sum(50, 10) == 150);
    assert!(sum(50, 25) == 75);
}

#[test]
fn solve_works() {
    assert!(solve(50, (10, 25)) == 175);
    assert!(solve(50, (25, 10)) == 175);
    assert!(solve(55, (10, 25)) == 175);
    assert!(solve(55, (25, 10)) == 175);
    assert!(solve(9, (3, 5)) == 23);
}
