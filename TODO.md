# ToDo

## General

- [ ] solve problems 50 up until 100
    - [x] solve 10 of the problems
    - [ ] solve 20 of the problems
    - [ ] solve 30 of the problems
    - [ ] solve 40 of the problems
    - [ ] solve 50 of the problems
- [ ] solve problems higher than 100
- [ ] make `solve_problem.py` tool add problem to `src/problem/mod.rs`.
- [ ] rework euler tool info output (JSON output)
- [ ] incorporate color output and ninja-like console stuff
- [ ] fix up to digits: reverse iterator, make output type generic, implement for i8
- [ ] remove legacy solver binaries
- [ ] https://users.rust-lang.org/t/how-to-fail-a-build-with-warnings/2687/8
- [ ] builder pattern for euler info to make it extendable
- [x] use generated results.json in CI to generate progress.html automatically
- [x] make problems 3 and 4 work
- [x] write script that uses results of solve to automatically migrate
- [x] port everything over
- [x] write euler tool, which can check solutions (measuring elapsed time in milliseconds, reports if either a solver or a solution is missing and can also display metadata about a problem)
- [x] make euler tool part of CI pipeline (maybe?)
- [x] move everything into one single crate with one single executable
- [x] make euler tool (or some other tool) publish data as HTML and host with gitlab pages
- [x] move even_fibonacci in p002 into util
- [x] switch to using less expensive hashes (cost factor 6 should be good, takes 10ms on raspi).

