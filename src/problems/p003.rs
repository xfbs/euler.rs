//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::is_prime;

pub const INFO: EulerInfo = EulerInfo {
    number: 3,
    title: "Largest prime factor",
    slug: "largest-prime-factor",
    solve: Some(solve_default),
    solution: Some("$2b$04$r8ndKxWW4WQNoZvuMkq3POSaAZD5Wf8fc7A8wIR/q/G2PDkRjsFlC"),
};

pub fn solve_default() -> String {
    solve(600_851_475_143).to_string()
}

pub fn solve(num: u64) -> u64 {
    let mut prime = 2;
    let mut cur = num;

    while !is_prime(cur) {
        while cur != prime && (cur % prime) == 0 {
            cur /= prime;
        }

        prime += 1;
        while !is_prime(prime) {
            prime += 1;
        }
    }

    cur
}

#[test]
fn solve_works() {
    assert!(solve(2) == 2);
    assert!(solve(6) == 3);
    assert!(solve(15) == 5);
    assert!(solve(32) == 2);
    assert!(solve(31) == 31);
    assert!(solve(39) == 13);
}
