//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::IsPalindrome;

pub const INFO: EulerInfo = EulerInfo {
    number: 4,
    title: "Largest palindrome product",
    slug: "largest-palindrome-product",
    solve: Some(solve_default),
    solution: Some("$2b$04$8Ur.Ior.hIWb04c8GVHjuOW.czfPoTAGO0dJtMuYk.5kjV0NZXy0C"),
};

pub fn solve_default() -> String {
    solve(3).to_string()
}

pub fn solve(n: u8) -> u32 {
    let min = 10_u32.pow((n - 1) as u32);
    let max = 10_u32.pow(n as u32);
    (min..max)
        .map(|i| {
            (min..max)
                .map(|j| i * j)
                .filter(|p| p.is_palindrome(10))
                .max()
                .unwrap_or(0)
        })
        .max()
        .unwrap()
}

#[test]
fn test_solve() {
    assert!(solve(2) == 9009);
}
