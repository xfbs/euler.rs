//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::ToDivisors;

pub const INFO: EulerInfo = EulerInfo {
    number: 23,
    title: "Non-abundant sums",
    slug: "non-abundant-sums",
    solve: Some(solve_default),
    solution: Some("$2b$04$XldPAph2gL6i/BhRDV/QQOiHp9iHHCMApRISIbr67XoA1SODB5Hei"),
};

pub fn solve_default() -> String {
    solve(28123).to_string()
}

pub fn solve(max: u64) -> u64 {
    let mut possible = vec![false; max as usize];

    let abundant = (1..max)
        .filter(|n| n.divisors().sum::<u64>() > *n)
        .collect::<Vec<u64>>();

    for a in 0..abundant.len() {
        for b in a..abundant.len() {
            let sum = abundant[a] + abundant[b];

            if sum >= max {
                break;
            }

            possible[sum as usize] = true;
        }
    }

    possible
        .iter()
        .enumerate()
        .filter(|&(_, poss)| !poss)
        .map(|(index, _)| index as u64)
        .sum()
}

#[test]
fn test_solve() {
    assert_eq!(solve(10), 45);
    assert_eq!(solve(20), 190);
    assert_eq!(solve(25), 276);
    assert_eq!(solve(32), 442);
}
