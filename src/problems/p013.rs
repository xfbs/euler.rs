//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 13,
    title: "Large sum",
    slug: "large-sum",
    solve: Some(solve_default),
    solution: Some("$2b$04$DWmHc8BjI8Fl3lB2uNa89erCWIJ97ek3fCPu1v/G5o04gVirj4sIS"),
};

pub const DATA: &'static str = include_str!("p013.txt");

pub fn solve_default() -> String {
    let numbers = DATA.lines().map(|i| i.to_owned()).collect::<Vec<String>>();

    solve(numbers.as_slice(), 10).to_string()
}

pub fn solve(nums: &[String], len: usize) -> u64 {
    let mut temp = nums
        .iter()
        .map(|i| i[..(len + 1)].parse::<u64>().unwrap())
        .fold(0, |m, c| m + c);

    while temp >= 10u64.pow(len as u32) {
        temp /= 10;
    }

    temp
}

#[test]
fn test_solve() {
    assert_eq!(
        solve(
            vec![String::from("12345678"), String::from("12345678")].as_slice(),
            5
        ),
        24691
    );

    assert_eq!(
        solve(
            vec![
                String::from("3414234792"),
                String::from("3414234792"),
                String::from("3414234792"),
                String::from("3414234792"),
                String::from("2341234798"),
            ]
            .as_slice(),
            5
        ),
        15998
    );
}
