//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::ToDigits;
use itertools::Itertools;

pub const INFO: EulerInfo = EulerInfo {
    number: 52,
    title: "Permuted multiple",
    slug: "permuted-multiple",
    solve: Some(solve_default),
    solution: Some("$2b$04$NmdUmUO.Eae1rIqAcToSv.wE0zb/Ip28AsqVgw0Fm7Hc4u1kRP4AW"),
};

pub fn solve_default() -> String {
    permuted_multiple(10, &[2, 3, 4, 5, 6]).unwrap().to_string()
}

pub fn permuted_multiple(base: u32, multipliers: &[u32]) -> Option<u32> {
    (1..u32::MAX).find(|&x| is_permuted_multiple(base, multipliers, x))
}

pub fn is_permuted_multiple(base: u32, multipliers: &[u32], number: u32) -> bool {
    let digits: Vec<u32> = number.digits().base(base).sorted().collect();

    for multiplier in multipliers.iter() {
        let new_digits = (number * multiplier).digits().base(base).sorted();

        if !digits.iter().map(|&x| x).eq(new_digits) {
            return false;
        }
    }

    true
}

#[test]
fn test_solve() {
    assert!(is_permuted_multiple(10, &[2], 125874));
}
