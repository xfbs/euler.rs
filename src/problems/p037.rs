//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;
use crate::util::ToDigits;

pub const INFO: EulerInfo = EulerInfo {
    number: 37,
    title: "Truncatable primes",
    slug: "truncatable-primes",
    solve: Some(solve_default),
    solution: Some("$2b$04$i.3t9nW6rQXsGwBJr5RtCuXsvN1AN89jl1bOpZd.HKjkwt7y6Ye4O"),
};

pub fn solve_default() -> String {
    solve(11).to_string()
}

pub fn solve(count: usize) -> u64 {
    // I had to use two prime generators here, because I can't mutibly borrow
    // one twice.. maybe this could be fixed somehow?
    let mut primes = Prime::new();
    let mut other = Prime::new();

    primes
        .into_iter()
        .skip(4)
        .filter(|&n| is_truncatable(&mut other, n))
        .take(count)
        .sum()
}

/// Checks if a prime number is truncatable from both sides.
///
/// # Examples
///
/// ```
/// # use xfbs_euler::util::Prime;
/// # use xfbs_euler::problems::p037::is_truncatable;
/// # fn main() {
/// let mut primes = Prime::new();
/// assert!(is_truncatable(&mut primes, 23));
/// assert!(!is_truncatable(&mut primes, 17));
/// # }
/// ```
pub fn is_truncatable(primes: &mut Prime, prime: u64) -> bool {
    let right = prime
        .digits()
        .scan(0, |memo, digit| {
            *memo *= 10;
            *memo += digit;
            Some(*memo)
        })
        .all(|n| primes.check(n));

    let left = prime
        .digits()
        .reverse()
        .scan((0, 1), |memo, digit| {
            memo.0 += memo.1 * digit;
            memo.1 *= 10;
            Some(memo.0)
        })
        .all(|n| primes.check(n));

    right && left
}

#[test]
fn test_is_truncatable() {
    let mut primes = Prime::new();

    assert!(is_truncatable(&mut primes, 3797));
    assert!(is_truncatable(&mut primes, 23));
    assert!(!is_truncatable(&mut primes, 2141));
}

#[test]
fn test_solve() {
    assert_eq!(solve(1), 23);
}
