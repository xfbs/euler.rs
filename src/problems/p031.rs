//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 31,
    title: "Coin sums",
    slug: "coin-sums",
    solve: Some(solve_default),
    solution: Some("$2b$04$Jlpw2SR.68z5crfRG5u2zO1frxbfDCMskdR0Lci40OppmRpY7Bvei"),
};

pub fn solve_default() -> String {
    solve(200).to_string()
}

pub fn solve(value: u32) -> usize {
    let coins = vec![200, 100, 50, 20, 10, 5, 2, 1];
    change(&coins, value, 0)
}

pub fn change(coins: &Vec<u32>, value: u32, pos: usize) -> usize {
    if value == 0 || (pos + 1) == coins.len() {
        if value % coins.last().unwrap() == 0 {
            1
        } else {
            0
        }
    } else {
        (0..(value / coins[pos] + 1))
            .map(|n| change(coins, value - n * coins[pos], pos + 1))
            .sum()
    }
}

#[test]
fn test_change() {
    assert_eq!(change(&vec![1], 5, 0), 1);
    assert_eq!(change(&vec![1], 2, 0), 1);

    assert_eq!(change(&vec![2, 1], 1, 0), 1);
    assert_eq!(change(&vec![2, 1], 2, 0), 2);
    assert_eq!(change(&vec![2, 1], 3, 0), 2);
    assert_eq!(change(&vec![2, 1], 4, 0), 3);

    assert_eq!(change(&vec![5, 2, 1], 5, 0), 4);
    assert_eq!(change(&vec![5, 2, 1], 6, 0), 5);

    assert_eq!(change(&vec![5, 2], 1, 0), 0);
    assert_eq!(change(&vec![5, 2], 2, 0), 1);
    assert_eq!(change(&vec![5, 2], 3, 0), 0);
    assert_eq!(change(&vec![5, 2], 4, 0), 1);
    assert_eq!(change(&vec![5, 2], 5, 0), 1);
}
