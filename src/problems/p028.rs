//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::SpiralDiagonals;

pub const INFO: EulerInfo = EulerInfo {
    number: 28,
    title: "Number spiral diagonals",
    slug: "number-spiral-diagonals",
    solve: Some(solve_default),
    solution: Some("$2b$04$JKB4XiJMAcDlgJg4E5caWO8CaPjAy5bNw6qAFiGTMpkvMkpnth6R2"),
};

pub fn solve_default() -> String {
    solve(1001).to_string()
}

pub fn solve(dim: u64) -> u64 {
    // width increases by two, and it's four diagonals per layer
    let items = 1 + (dim - 1) * 2;

    SpiralDiagonals::<u64>::new().take(items as usize).sum()
}

#[test]
fn test_solve() {
    assert_eq!(solve(3), 25);
    assert_eq!(solve(5), 101);
}
