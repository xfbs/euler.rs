//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::ToDigits;
use std::collections::HashMap;

pub const INFO: EulerInfo = EulerInfo {
    number: 92,
    title: "Square digit chains",
    slug: "square-digit-chains",
    solve: Some(solve_default),
    solution: Some("$2a$10$RO7aQEQYdS5VSkM1fbMCkuoLFh1yFZ6VWriVFFP69RyYjq8hyAhhG"),
};

pub fn solve_default() -> String {
    solve(10_000_000).to_string()
}

pub fn solve(max: u64) -> usize {
    let mut c = SquareDigitCache::new();

    (1..max)
        .map(|n| c.check(n))
        .filter(|r| *r == SquareDigitEnd::EightyNine)
        .count()
}

pub fn square_digit(number: u64) -> u64 {
    number.digits().map(|digit| digit.pow(2)).sum()
}

#[derive(Debug, Clone, PartialEq)]
pub struct SquareDigitCache {
    cache: HashMap<u64, SquareDigitEnd>,
}

impl SquareDigitCache {
    pub fn new() -> SquareDigitCache {
        let mut cache = HashMap::new();
        cache.insert(1, SquareDigitEnd::One);
        cache.insert(89, SquareDigitEnd::EightyNine);
        SquareDigitCache { cache: cache }
    }

    pub fn check(&mut self, number: u64) -> SquareDigitEnd {
        if let Some(end) = self.cache.get(&number) {
            return *end;
        }

        let ret = self.check(square_digit(number));
        self.cache.insert(number, ret);
        ret
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SquareDigitEnd {
    One,
    EightyNine,
}

#[test]
fn test_square_digit() {
    assert_eq!(square_digit(44), 32);
    assert_eq!(square_digit(32), 13);
    assert_eq!(square_digit(13), 10);
    assert_eq!(square_digit(10), 1);
    assert_eq!(square_digit(1), 1);

    assert_eq!(square_digit(85), 89);
    assert_eq!(square_digit(89), 145);
    assert_eq!(square_digit(145), 42);
    assert_eq!(square_digit(42), 20);
    assert_eq!(square_digit(20), 4);
    assert_eq!(square_digit(4), 16);
    assert_eq!(square_digit(16), 37);
    assert_eq!(square_digit(37), 58);
    assert_eq!(square_digit(58), 89);
}

#[test]
fn test_square_digit_cache() {
    let mut c = SquareDigitCache::new();
    assert_eq!(c.check(44), SquareDigitEnd::One);
    assert_eq!(c.check(32), SquareDigitEnd::One);
    assert_eq!(c.check(13), SquareDigitEnd::One);
    assert_eq!(c.check(10), SquareDigitEnd::One);
    assert_eq!(c.check(1), SquareDigitEnd::One);

    assert_eq!(c.check(85), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(89), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(58), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(37), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(16), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(4), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(20), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(42), SquareDigitEnd::EightyNine);
    assert_eq!(c.check(145), SquareDigitEnd::EightyNine);
}
