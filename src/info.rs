use std::fmt;

#[derive(Copy, Clone, Debug)]
pub struct EulerInfo {
    pub number: usize,
    pub title: &'static str,
    pub slug: &'static str,
    pub solve: Option<fn() -> String>,
    pub solution: Option<&'static str>,
}

impl fmt::Display for EulerInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "number: {}\n", self.number)?;
        write!(f, "title: {}\n", self.title)?;
        match self.solve {
            Some(_) => write!(f, "solve: yes"),
            None => write!(f, "solve: no"),
        }
    }
}
