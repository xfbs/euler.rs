//! # Euler
//!
//! This crate is part of [xfbs/euler](https://xfbs.github.io/euler), which aims
//! to solve [Project Euler](https://projecteuler.net/) problems in a variety of
//! languages.
//!
//! The functions and traits of this crate are sharedbetween solutions
//! solutions and implement generic, reusable algorithms. Tests should be
//! available for all functions.
//!
//! If you clone the [repository](https://github.com/xfbs/euler), and navigate
//! to this library (which is in `/lib/rust/euler`), there is a `Makefile` that
//! supports the following targets:
//!
//!   - `make test` to build and run all the tests of this library.
//!
//!   - `make fmt` to run `cargo fmt`, the rust code formatter, over all of the
//!     code in this library.
//!
//!   - `make doc` to generates these docs. The output will be placed in
//!     `/doc/lib/rust` in the repository.
//!
//! All code in this library has been developed and tested with Rust `1.22.1`.
//! Since rust is still under development, things may break with future releases
//! of the compiler.

pub mod digits;
pub mod divisors;
pub mod even_fibonacci;
pub mod is_prime;
pub mod math;
pub mod modular;
pub mod palindrome;
pub mod permutations;
pub mod prime;
pub mod reverse;
pub mod rotations;
pub mod sieve;
pub mod spiral_diagonals;

pub use crate::util::digits::Digits;
pub use crate::util::digits::ToDigits;
pub use crate::util::divisors::ToDivisors;
pub use crate::util::even_fibonacci::EvenFibonacci;
pub use crate::util::is_prime::is_prime;
pub use crate::util::math::factorial;
pub use crate::util::math::Gcd;
pub use crate::util::math::Lcm;
pub use crate::util::palindrome::IsPalindrome;
pub use crate::util::palindrome::ToPalindrome;
pub use crate::util::permutations::permutations;
pub use crate::util::permutations::Permutations;
pub use crate::util::prime::Prime;
pub use crate::util::reverse::Reverse;
pub use crate::util::rotations::Rotations;
pub use crate::util::rotations::ToRotations;
pub use crate::util::sieve::Sieve;
pub use crate::util::spiral_diagonals::SpiralDiagonals;
