//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::IsPalindrome;
use crate::util::ToPalindrome;
use std::iter::once;

pub const INFO: EulerInfo = EulerInfo {
    number: 36,
    title: "Double-base palindromes",
    slug: "double-base-palindromes",
    solve: Some(solve_default),
    solution: Some("$2b$04$Wrvt7p84eOCTEid5iRkKF.t7OO34tCgmSKB5RZFOIXL.nqPsPYFN6"),
};

pub fn solve_default() -> String {
    solve(1000000).to_string()
}

pub fn solve(max: u32) -> u32 {
    (1..)
        .map(|n| n.to_palindrome(10))
        .take_while(|&(small, _big)| small < max)
        .flat_map(|(small, big)| once(small).chain(once(big)))
        .filter(|&n| n < max)
        .filter(|&n| n.is_palindrome(2))
        .sum()
}

#[test]
fn test_solve() {
    assert_eq!(solve(10), 1 + 3 + 5 + 7 + 9);
}

#[test]
fn test_is_palindrome() {
    assert!(585.is_palindrome(10));
    assert!(585.is_palindrome(2));
    assert!(!584.is_palindrome(10));
    assert!(!584.is_palindrome(2));
}
