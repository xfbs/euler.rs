//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 39,
    title: "Integer right triangles",
    slug: "integer-right-triangles",
    solve: Some(solve_default),
    solution: Some("$2b$04$6D3nBXYvEa3RfkG1trQOSOu94AcvvB3z.u3mpm2syfa8vE5Cg9fjO"),
};

pub fn solve_default() -> String {
    solve(1000).to_string()
}

pub fn solve(max: u32) -> u32 {
    (1..(max + 1))
        .map(|p| (p, solutions_count(p)))
        .max_by_key(|t| t.1)
        .unwrap()
        .0
}

pub fn solutions_count(p: u32) -> u32 {
    let p: i32 = p as i32;
    (1..(p / 3))
        .filter(|a| ((p * (2 * a - p)) % (2 * (a - p))) == 0)
        .count() as u32
}

#[test]
fn test_solutions_count() {
    assert_eq!(solutions_count(120), 3);
    assert_eq!(solutions_count(12), 1);
}

#[test]
fn test_solve() {
    assert_eq!(solve(12), 12);
}
