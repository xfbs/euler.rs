import shutil
import sys
import glob
import os
import re

problem = int(sys.argv[1])
problem_just = str(problem).rjust(3, '0')
problem_pnum = "p" + problem_just
unsolved = glob.glob("unsolved/{}-*".format(problem_just))

if len(unsolved) == 0:
    print("error: problem {} not in unsolved!".format(problem))
    sys.exit()

unsolved = unsolved[0]

print("located problem {} at {}.".format(problem, unsolved))

problemdir = "src/problems/p{}".format(problem_just)

shutil.move(unsolved, problemdir)

print("moved over to {}.".format(problemdir))

modrs = os.path.join(problemdir, "mod.rs")
mod = open(modrs, mode='w')

mod.write("use crate::info::EulerInfo;\n\n")

descfile = open(os.path.join(problemdir, "problem.md")).read()

title = ' '.join(descfile.split("\n")[0].split(" ")[3:])
print("title is {}".format(title))

slug = re.sub("\s", '-', title.lower())
print("slug is {}".format(slug))

datafiles = glob.glob(os.path.join(problemdir, "*.txt"))

mod.write("pub const INFO: EulerInfo = EulerInfo {\n")
mod.write("    number: {},\n".format(problem))
mod.write("    title: \"{}\",\n".format(title))
mod.write("    slug: \"{}\",\n".format(slug))
mod.write("    solve: None,\n")
mod.write("    solution: Some(include_str!(\"solution.bcrypt\")),\n")
mod.write("    description: Some(include_str!(\"problem.md\")),\n")
mod.write("};\n")

if len(datafiles) != 0:
    print("located data files {}.".format(datafiles))
    filename = os.path.basename(datafiles[0])
    mod.write("\npub const DATA: &'static str = include_str!(\"{}\");\n".format(filename))

print("created {} with info.".format(modrs))
