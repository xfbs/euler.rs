//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::problems::p018::solve;

pub const INFO: EulerInfo = EulerInfo {
    number: 67,
    title: "Maximum path sum II",
    slug: "maximum-path-sum-ii",
    solve: Some(solve_default),
    solution: Some("$2b$04$TzNHNQjW2HwnMWA987j22.Q2/dney.MhUsUz8v72fpdp94fwlN6l2"),
};

pub const DATA: &'static str = include_str!("p067.txt");

pub fn solve_default() -> String {
    let numbers = DATA
        .lines()
        .map(|i| {
            i.split(' ')
                .map(|n| n.parse::<u8>().unwrap())
                .collect::<Vec<u8>>()
        })
        .collect::<Vec<Vec<u8>>>();

    solve(&numbers).to_string()
}
