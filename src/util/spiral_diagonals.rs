pub struct SpiralDiagonals<T> {
    ring: T,
    cur: T,
    pos: u8,
}

impl<T> SpiralDiagonals<T>
where
    T: std::ops::AddAssign + From<u8> + Copy,
{
    pub fn new() -> SpiralDiagonals<T> {
        SpiralDiagonals::<T> {
            ring: 0.into(),
            cur: 1.into(),
            pos: 4.into(),
        }
    }

    pub fn next(&mut self) {
        if self.pos == 4 {
            self.ring += 2.into();
            self.pos = 0;
        }

        self.cur += self.ring;
        self.pos += 1;
    }

    pub fn get(&self) -> T {
        self.cur
    }

    pub fn side_length(&self) -> T {
        let mut ring = self.ring;
        ring += 1.into();
        ring
    }

    pub fn is_beginning(&self) -> bool {
        self.pos == 1
    }
}

impl<T> Iterator for SpiralDiagonals<T>
where
    T: std::ops::AddAssign + From<u8> + Copy,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.get();
        self.next();
        Some(ret)
    }
}
