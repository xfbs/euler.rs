//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;

pub const INFO: EulerInfo = EulerInfo {
    number: 12,
    title: "Highly divisible triangle number",
    slug: "highly-divisible-triange-number",
    solve: Some(solve_default),
    solution: Some("$2b$04$JOBDgr3/W1.SdIEAvnpRXec6f9CocznwrtJxucbFf2yepMgpXl1Rm"),
};

pub fn solve_default() -> String {
    solve(500).to_string()
}

pub fn solve(max: u32) -> u32 {
    let mut triangle = 1;
    let mut index = 1;
    let mut primes = Prime::new();

    while factors(&mut primes, triangle) <= max {
        index += 1;
        triangle += index;
    }

    triangle
}

fn factors(primes: &mut Prime, num: u32) -> u32 {
    let mut num = num;
    let mut count = 1;
    let mut cur = 1;

    while num != 1 {
        let mut factors = 1;
        let prime = primes.nth(cur);

        while (num % (prime as u32)) == 0 {
            factors += 1;
            num /= prime as u32;
        }

        count *= factors;
        cur += 1;
    }

    count
}

#[test]
fn test_solve() {
    assert_eq!(solve(1), 3);
    assert_eq!(solve(2), 6);
    assert_eq!(solve(4), 28);
}

#[test]
fn test_factors() {
    let mut p = Prime::new();

    assert_eq!(factors(&mut p, 1), 1);
    assert_eq!(factors(&mut p, 2), 2);
    assert_eq!(factors(&mut p, 3), 2);
    assert_eq!(factors(&mut p, 4), 3);
    assert_eq!(factors(&mut p, 5), 2);
    assert_eq!(factors(&mut p, 6), 4);
    assert_eq!(factors(&mut p, 7), 2);
    assert_eq!(factors(&mut p, 8), 4);
    assert_eq!(factors(&mut p, 9), 3);
    assert_eq!(factors(&mut p, 10), 4);
    assert_eq!(factors(&mut p, 15), 4);
    assert_eq!(factors(&mut p, 21), 4);
    assert_eq!(factors(&mut p, 28), 6);
}
