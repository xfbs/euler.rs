use crate::problems;
use crate::tool::Subcommand;
use crate::tool::TestChoice;
use clap::{App, AppSettings, Arg, ArgMatches};
use serde::{Deserialize, Serialize};
use std::fs::OpenOptions;
use std::path::PathBuf;
use std::time::{Duration, Instant};

#[derive(Clone, Debug)]
pub struct Check {
    test: TestChoice,
    timeout: Duration,
    results: Option<PathBuf>,
}

impl Subcommand for Check {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("check")
            .about("Checks solution to project euler problems")
            .setting(AppSettings::ArgRequiredElseHelp)
            .arg(
                Arg::with_name("all")
                    .help("Checks all problems")
                    .long("all")
                    .short("a"),
            )
            .arg(
                Arg::with_name("timeout")
                    .long("timeout")
                    .short("t")
                    .help("Timeout")
                    .takes_value(true)
                    .value_name("TIME")
                    .default_value("1s")
                    .validator(|x| {
                        x.parse::<humantime::Duration>()
                            .map(|_| ())
                            .map_err(|e| e.to_string())
                    }),
            )
            .arg(
                Arg::with_name("results")
                    .long("results")
                    .short("r")
                    .help("File to write results to as JSON")
                    .takes_value(true)
                    .value_name("FILE"),
            )
            .arg(
                Arg::with_name("number")
                    .help("The problem to check")
                    .required_unless("all")
                    .index(1),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        Check {
            test: parse_test_choice(matches),
            timeout: matches
                .value_of("timeout")
                .unwrap()
                .parse::<humantime::Duration>()
                .ok()
                .unwrap()
                .into(),
            results: parse_results_file(matches),
        }
    }

    fn run(&self) -> Result<(), String> {
        match self.test {
            TestChoice::All => check_all(self.clone()),
            TestChoice::Test(x) => check_number(x),
        }

        Ok(())
    }
}

#[derive(Serialize, Deserialize)]
struct CheckResult {
    name: String,
    number: usize,
    result: bool,
    time: Duration,
}

fn check_all(args: Check) {
    let mut all_good = true;
    let mut good = 0;
    let mut bad = 0;
    let mut results: Vec<CheckResult> = Vec::new();

    for (i, problem) in problems::PROBLEMS.iter().enumerate() {
        match (problem.solve, problem.solution) {
            (Some(solver), Some(solution)) => {
                println!(
                    "[{}/{}] checking problem {}: {}",
                    i + 1,
                    problems::PROBLEMS.len(),
                    problem.number,
                    problem.title
                );
                // run solver and measure time
                let start = Instant::now();
                let result = solver();
                let duration = start.elapsed();

                if duration > args.timeout {
                    println!("  took {}ms", duration.as_millis());
                }

                // check result
                let result = bcrypt::verify(result, solution);
                let result = result.unwrap();
                if result {
                    good += 1;
                } else {
                    println!("=> error, wrong");
                    bad += 1;
                    all_good = false;
                }

                let result = CheckResult {
                    name: problem.title.to_string(),
                    number: problem.number,
                    result: result,
                    time: duration,
                };

                results.push(result);
            }
            _ => println!(
                "[{}/{}] skipping {} (no solver or solution)",
                i + 1,
                problems::PROBLEMS.len(),
                problem.number,
            ),
        }
    }

    println!("=> {}/{} problems correct", good, good + bad);

    match args.results {
        Some(path) => {
            let file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(path)
                .unwrap();
            serde_json::to_writer(file, &results).ok().unwrap();
        }
        None => {}
    }

    if !all_good {
        std::process::exit(1);
    }
}

fn check_number(number: usize) {
    match problems::PROBLEMS.get(number - 1) {
        Some(x) => match (x.solve, x.solution) {
            (Some(solver), Some(solution)) => {
                let result = solver();
                let result = bcrypt::verify(result, solution);
                if result.unwrap() {
                    println!("all good!");
                } else {
                    println!("oops, wrong!");
                }
            }
            (None, Some(_)) => println!("error: unsolved"),
            (Some(_), None) => println!("error: no solution"),
            (None, None) => println!("error: unsolved and no solution"),
        },
        None => println!("error: not found"),
    }
}

fn parse_results_file(a: &ArgMatches) -> Option<PathBuf> {
    a.value_of("results").map(|x| PathBuf::from(x))
}

fn parse_test_choice(a: &ArgMatches) -> TestChoice {
    if a.is_present("all") {
        TestChoice::All
    } else {
        let number = a
            .value_of("number")
            .and_then(|x| x.parse::<usize>().ok())
            .unwrap();
        TestChoice::Test(number)
    }
}
