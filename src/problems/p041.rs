//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::factorial;
use crate::util::permutations;
use crate::util::Prime;

pub const INFO: EulerInfo = EulerInfo {
    number: 41,
    title: "Pandigital prime",
    slug: "pandigital-prime",
    solve: Some(solve_default),
    solution: Some("$2b$04$QQ/iztBuS.cBtOlTbeesyOTJCpaEx5UfbZ2IZtIGTDajpTa4NMXrm"),
};

pub fn solve_default() -> String {
    solve(9).to_string()
}

pub fn solve(digit: u8) -> u64 {
    let mut primes = Prime::new();

    for n_iter in 0..digit {
        let n = digit - n_iter;
        let fact = factorial(n as u64).unwrap();

        for nth_iter in 0..fact {
            let nth = fact - nth_iter - 1;

            let num = permutations(n as usize, nth as usize)
                .map(|p| p + 1)
                .fold(0, |m, c| 10 * m + c as u64);

            if primes.check(num) {
                return num;
            }
        }
    }

    0
}

#[test]
fn test_solve() {
    let solve_4 = solve(4);
    assert!(solve_4 > 2143);

    let solve_5 = solve(5);
    assert!(solve_5 <= 54321);
    assert!(solve_4 <= solve_5);

    let solve_6 = solve(6);
    assert!(solve_6 <= 654321);
    assert!(solve_5 <= solve_6);

    let solve_7 = solve(7);
    assert!(solve_7 <= 7654321);
    assert!(solve_6 <= solve_7);

    let solve_8 = solve(8);
    assert!(solve_8 <= 87654321);
    assert!(solve_7 <= solve_8);
}
