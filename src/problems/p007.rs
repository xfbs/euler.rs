//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Prime;

pub const INFO: EulerInfo = EulerInfo {
    number: 7,
    title: "10001st prime",
    slug: "10001st-prime",
    solve: Some(solve_default),
    solution: Some("$2b$04$70bV1L/SkcNNy4dTx6YyJOlhwlfhScbERDURUGqJxNp6Si6mFQvr2"),
};

pub fn solve_default() -> String {
    solve(10001).to_string()
}

pub fn solve(nth: usize) -> u64 {
    let mut p = Prime::new();
    p.nth(nth)
}

#[test]
fn test_solve() {
    assert!(solve(1) == 2);
    assert!(solve(2) == 3);
    assert!(solve(3) == 5);
    assert!(solve(4) == 7);
    assert!(solve(5) == 11);
    assert!(solve(6) == 13);
}
