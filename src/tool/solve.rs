use crate::problems;
use crate::tool::HashChoice;
use crate::tool::Subcommand;
use crate::tool::TestChoice;
use clap::{App, AppSettings, Arg, ArgMatches};
use serde::{Deserialize, Serialize};
use std::fs::{File, OpenOptions};
use std::path::PathBuf;

#[derive(Debug)]
pub struct Solve {
    test: TestChoice,
    hash: HashChoice,
    results: Option<File>,
}

#[derive(Serialize, Deserialize)]
struct SolveResult {
    name: String,
    number: usize,
    output: String,
}

impl Subcommand for Solve {
    fn app<'a, 'b>() -> App<'a, 'b> {
        App::new("solve")
            .about("Solves a given project euler problem")
            .setting(AppSettings::ArgRequiredElseHelp)
            .arg(
                Arg::with_name("all")
                    .help("Checks all problems")
                    .long("all")
                    .short("a"),
            )
            .arg(
                Arg::with_name("bcrypt")
                    .long("bcrypt")
                    .help("The problem to check")
                    .takes_value(true)
                    .value_name("COST")
                    .default_value("4")
                    .validator(|x| x.parse::<u32>().map(|_| ()).map_err(|e| e.to_string())),
            )
            .arg(
                Arg::with_name("results")
                    .long("results")
                    .short("r")
                    .help("File to write results to as JSON")
                    .takes_value(true)
                    .value_name("FILE"),
            )
            .arg(
                Arg::with_name("number")
                    .help("The problem to check")
                    .required_unless("all")
                    .index(1),
            )
    }

    fn parse(matches: &ArgMatches) -> Self {
        Solve {
            test: parse_test_choice(matches),
            hash: parse_hash_choice(matches),
            results: parse_results_file(matches)
                .and_then(|path| OpenOptions::new().write(true).create(true).open(path).ok()),
        }
    }

    fn run(&self) -> Result<(), String> {
        let mut results: Vec<SolveResult> = Vec::new();

        for problem in problems::PROBLEMS.iter() {
            match problem.solve {
                Some(solver) => {
                    let mut result = solver();

                    match self.hash {
                        HashChoice::BCrypt(cost) => {
                            result = bcrypt::hash(result, cost).unwrap();
                            println!("problem {}: {:?}", problem.number, result);
                        }
                        HashChoice::None => println!("problem {}: {}", problem.number, result),
                    }

                    results.push(SolveResult {
                        name: problem.title.to_string(),
                        number: problem.number,
                        output: result,
                    });
                }
                _ => {}
            }
        }

        match &self.results {
            Some(file) => serde_json::to_writer(file, &results).ok().unwrap(),
            _ => {}
        }

        Ok(())
    }
}

fn parse_test_choice(a: &ArgMatches) -> TestChoice {
    if a.is_present("all") {
        TestChoice::All
    } else {
        let number = a
            .value_of("number")
            .and_then(|x| x.parse::<usize>().ok())
            .unwrap();
        TestChoice::Test(number)
    }
}

fn parse_hash_choice(a: &ArgMatches) -> HashChoice {
    if a.occurrences_of("bcrypt") > 0 {
        let cost = a.value_of("bcrypt").unwrap().parse::<u32>().unwrap_or(4);
        HashChoice::BCrypt(cost)
    } else {
        HashChoice::None
    }
}

fn parse_results_file(a: &ArgMatches) -> Option<PathBuf> {
    a.value_of("results").map(|x| PathBuf::from(x))
}
