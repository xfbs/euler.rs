//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 53,
    title: "Combinatoric selections",
    slug: "combinatoric-selections",
    solve: Some(solve_default),
    solution: Some("$2b$04$u5Hi1SCK5lU796d.SJf8u.392Q6IIEUZKib2ogrkr/dDspiaWa1I2"),
};

#[derive(Copy, Clone, Debug)]
pub struct CombinatoricSelections {
    r: u32,
    n: u32,
    value: u64,
}

pub fn solve_default() -> String {
    count_over(1000000, 100).to_string()
}

impl CombinatoricSelections {
    pub fn new(n: u32) -> CombinatoricSelections {
        CombinatoricSelections {
            r: 0,
            n: n,
            value: 1,
        }
    }

    pub fn next_r(&mut self) {
        self.r += 1;
        self.value *= (self.n - self.r + 1) as u64;
        self.value /= self.r as u64;
    }

    pub fn prev_r(&mut self) {
        self.value *= self.r as u64;
        self.value /= (self.n - self.r + 1) as u64;
        self.r -= 1;
    }

    pub fn next_n(&mut self) {
        self.n += 1;
        self.value *= self.n as u64;
        self.value /= (self.n - self.r) as u64;
    }
}

fn min_over(value: u64) -> CombinatoricSelections {
    let mut c = CombinatoricSelections::new(1);

    loop {
        // find middle point
        while c.r < (c.n / 2) {
            if c.value > value {
                break;
            }

            c.next_r();
        }

        if c.value > value {
            break;
        }

        c.next_n();
    }

    while c.value > value {
        c.prev_r();
    }

    c.next_r();

    c
}

fn count_over(value: u64, max_n: u32) -> usize {
    let mut c = min_over(value);
    let mut count = 0;

    while c.n <= max_n {
        count += (c.n - 2 * c.r + 1) as usize;

        c.next_n();

        while c.value > value {
            c.prev_r();
        }

        c.next_r();
    }

    count
}

#[test]
fn test_count_over() {
    assert_eq!(count_over(1000000, 23), 4);
    assert_eq!(count_over(1000000, 24), 4 + 7);
    assert_eq!(count_over(1000000, 25), 4 + 7 + 10);
}

#[test]
fn test_min_over() {
    let c = min_over(1000000);
    assert_eq!(c.n, 23);
    assert_eq!(c.r, 10);
    assert_eq!(c.value, 1144066);

    let c = min_over(100);
    assert_eq!(c.n, 9);
    assert_eq!(c.r, 4);
    assert_eq!(c.value, 126);
}

#[test]
fn test_combinatoric_selections_iterator_five() {
    let mut c = CombinatoricSelections::new(5);

    c.next_r();
    assert_eq!(c.value, 5);
    c.next_r();
    assert_eq!(c.value, 10);
    c.next_r();
    assert_eq!(c.value, 10);
    c.next_r();
    assert_eq!(c.value, 5);
    c.next_r();
    assert_eq!(c.value, 1);

    c.prev_r();
    assert_eq!(c.value, 5);
    c.prev_r();
    assert_eq!(c.value, 10);
    c.prev_r();
    assert_eq!(c.value, 10);
    c.prev_r();
    assert_eq!(c.value, 5);
    c.prev_r();
    assert_eq!(c.value, 1);
}

#[test]
fn test_combinatoric_selections_iterator_three() {
    let mut c = CombinatoricSelections::new(3);

    c.next_r();
    assert_eq!(c.value, 3);
    c.next_r();
    assert_eq!(c.value, 3);
    c.next_r();
    assert_eq!(c.value, 1);

    c.prev_r();
    assert_eq!(c.value, 3);
    c.prev_r();
    assert_eq!(c.value, 3);
    c.prev_r();
    assert_eq!(c.value, 1);
}

#[test]
fn test_combinatoric_selections_next_r() {
    let mut c = CombinatoricSelections::new(3);
    c.next_r();
    c.next_r();
    assert_eq!(c.value, 3);
    c.next_n();
    assert_eq!(c.value, 6);
    c.next_n();
    assert_eq!(c.value, 10);
    c.next_n();
    assert_eq!(c.value, 15);
}
