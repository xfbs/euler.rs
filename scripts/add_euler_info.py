#!/usr/bin/env python3

import os
import re
import sys

problems_dir=sys.argv[1]

problems = os.listdir(problems_dir)

title_regex = re.compile(r"# Problem \d+: (.+)")
eulerinfo_regex = re.compile(r"EulerInfo")
problem_regex = re.compile(r"p\d\d\d")

for problem in problems:
    if problem_regex.match(problem) == None:
        continue
    number = int(problem[1:])
    desc = open(os.path.join(problems_dir, problem, "problem.md")).read().split("\n")
    heading = desc[0]
    title = title_regex.match(heading).group(1)
    slug = title.lower()
    slug = re.sub(r"\s", "-", slug)
    src = open(os.path.join(problems_dir, problem, "mod.rs")).read()
    if eulerinfo_regex.search(src) == None:
        print("patching {}: {}".format(number, title))

        out = "use crate::info::EulerInfo;\n\n"
        out += "pub const INFO: EulerInfo = EulerInfo {\n"
        out += "    number: {},\n".format(number)
        out += "    title: \"{}\",\n".format(title)
        out += "    slug: \"{}\",\n".format(slug)
        out += "    solve: None,\n"
        out += "    solution: Some(include_str!(\"solution.bcrypt\")),\n"
        out += "    description: Some(include_str!(\"problem.md\")),\n"
        out += "};\n\n"

        file = open(os.path.join(problems_dir, problem, "mod.rs"), mode='w')
        file.write(out)
        file.write(src)


