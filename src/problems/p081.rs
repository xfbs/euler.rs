//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use std::collections::VecDeque;

pub const INFO: EulerInfo = EulerInfo {
    number: 81,
    title: "Path sum two ways",
    slug: "path-sum-two-ways",
    solve: Some(solve_default),
    solution: Some("$2b$04$ZtvoVuC/.D/VUnIembp/0utAkWYdCwg8pZM55VY4mbcdixpp11aHC"),
};

pub const DATA: &'static str = include_str!("p081.txt");

pub fn solve_default() -> String {
    let data = DATA
        .lines()
        .map(|line| {
            line.split(",")
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    path_sum_two_ways(&data).to_string()
}

pub fn path_sum_two_ways(data: &Vec<Vec<u32>>) -> u32 {
    let rows = data.len();
    let cols = data.get(0).map(|n| n.len()).unwrap_or(0);

    let mut transposed = (0..cols)
        .map(|col| (0..rows).map(|row| data[row][col]).collect::<Vec<u32>>())
        .collect::<VecDeque<Vec<u32>>>();

    for i in 1..transposed[0].len() {
        transposed[0][i] += transposed[0][i - 1];
    }

    while transposed.len() > 1 {
        transposed[1][0] += transposed[0][0];

        for i in 1..transposed[1].len() {
            if transposed[0][i] < transposed[1][i - 1] {
                transposed[1][i] += transposed[0][i];
            } else {
                transposed[1][i] += transposed[1][i - 1];
            }
        }

        transposed.pop_front();
    }

    *transposed[0].last().unwrap()
}

#[test]
fn test_path_sum_two_ways_one_col() {
    assert_eq!(path_sum_two_ways(&vec![vec![4]]), 4);
    assert_eq!(path_sum_two_ways(&vec![vec![4, 2]]), 4 + 2);
    assert_eq!(path_sum_two_ways(&vec![vec![4], vec![2]]), 4 + 2);

    assert_eq!(path_sum_two_ways(&vec![vec![4, 9], vec![3, 7]]), 4 + 3 + 7);
    assert_eq!(path_sum_two_ways(&vec![vec![4, 2], vec![3, 7]]), 4 + 2 + 7);

    assert_eq!(
        path_sum_two_ways(&vec![vec![4, 2, 1], vec![3, 7, 4]]),
        4 + 2 + 1 + 4
    );
    assert_eq!(
        path_sum_two_ways(&vec![vec![4, 2, 4], vec![3, 3, 4]]),
        4 + 2 + 3 + 4
    );
    assert_eq!(
        path_sum_two_ways(&vec![vec![4, 9, 4], vec![3, 3, 4]]),
        4 + 3 + 3 + 4
    );
}
