//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use std::u32;

pub const INFO: EulerInfo = EulerInfo {
    number: 30,
    title: "Digit fifth powers",
    slug: "digit-fifth-powers",
    solve: Some(solve_default),
    solution: Some("$2b$04$Q70cyEXfaoiN27yFNaY4cO4wdwmIDRANCDCnkHDnz5pa.ePoPNO5G"),
};

pub fn solve_default() -> String {
    solve(354294).to_string()
}

pub fn solve(max: u32) -> u32 {
    let fifth_powers = (0u32..10).map(|n| n.pow(5)).collect::<Vec<u32>>();

    (3..(max + 1))
        .filter(|n| is_curious(&fifth_powers, *n))
        .sum()
}

pub fn is_curious(digit_mapping: &[u32], num: u32) -> bool {
    let mut sum = 0;
    let mut cur = num;

    while sum < num && 0 < cur {
        let digit = cur % 10;
        sum += digit_mapping[digit as usize];
        cur /= 10;
    }

    (cur == 0) && (sum == num)
}

#[test]
fn test_is_curious() {
    let fourth_powers = (0u32..10).map(|n| n.pow(4)).collect::<Vec<u32>>();

    assert!(is_curious(&fourth_powers, 1634));
    assert!(is_curious(&fourth_powers, 8208));
    assert!(is_curious(&fourth_powers, 9474));
}
