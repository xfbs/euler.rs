//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;

pub const INFO: EulerInfo = EulerInfo {
    number: 83,
    title: "Path sum: four ways",
    slug: "path-sum:-four-ways",
    solve: Some(solve_default),
    solution: Some("$2a$10$Egy5NOwLMielj47raaFbV.vQZZL4dQP1V77iO/0CRFU1ffd8WwUbK"),
};

pub const DATA: &'static str = include_str!("p083.txt");

pub fn solve_default() -> String {
    let data = DATA
        .lines()
        .map(|line| {
            line.split(",")
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();

    path_sum_four_ways(&data).to_string()
}

use std::iter::*;
pub fn dirs(
    x: usize,
    y: usize,
) -> FilterMap<std::ops::Range<usize>, Box<dyn Fn(usize) -> Option<(usize, usize)>>> {
    (0..4).filter_map(Box::new(move |n: usize| {
        (match n {
            0 if y > 0 => Some((x, y - 1)),
            1 => Some((x + 1, y)),
            2 => Some((x, y + 1)),
            3 if x > 0 => Some((x - 1, y)),
            _ => None,
        }) as Option<(usize, usize)>
    }) as Box<dyn Fn(usize) -> Option<(usize, usize)>>)
}

pub fn path_sum_four_ways(data: &Vec<Vec<u32>>) -> u32 {
    let mut height_distance = 0;
    let mut distances: Vec<Vec<u32>> = data
        .iter()
        .map(|row| {
            let mut row_distance = height_distance;
            height_distance += row[0];
            row.iter()
                .map(|n| {
                    row_distance += n;
                    row_distance
                })
                .collect()
        })
        .collect();

    let mut changed;
    loop {
        changed = false;
        for row in 0..distances.len() {
            for col in 0..distances[row].len() {
                let new = dirs(row, col)
                    .filter_map(|(r, c)| {
                        distances
                            .get(r)
                            .and_then(|r| r.get(c))
                            .map(|d| d + data[row][col])
                    })
                    .min()
                    .unwrap_or(distances[row][col]);

                if new < distances[row][col] {
                    changed = true;
                    distances[row][col] = new;
                }
            }
        }

        if !changed {
            break;
        }
    }

    *distances.last().unwrap().last().unwrap()
}

#[test]
fn test_dirs() {
    assert_eq!(
        dirs(1, 1).collect::<Vec<(usize, usize)>>(),
        vec![(1, 0), (2, 1), (1, 2), (0, 1)]
    );
    assert_eq!(
        dirs(0, 0).collect::<Vec<(usize, usize)>>(),
        vec![(1, 0), (0, 1)]
    );
    assert_eq!(
        dirs(1, 0).collect::<Vec<(usize, usize)>>(),
        vec![(2, 0), (1, 1), (0, 0)]
    );
    assert_eq!(
        dirs(0, 1).collect::<Vec<(usize, usize)>>(),
        vec![(0, 0), (1, 1), (0, 2)]
    );
}

#[test]
fn test_path_sum_four_ways() {
    assert_eq!(path_sum_four_ways(&vec![vec![5]]), 5);
    assert_eq!(path_sum_four_ways(&vec![vec![5, 6]]), 11);
    assert_eq!(path_sum_four_ways(&vec![vec![5], vec![6]]), 11);

    assert_eq!(path_sum_four_ways(&vec![vec![4, 2], vec![3, 5]]), 11);
    assert_eq!(path_sum_four_ways(&vec![vec![4, 3], vec![2, 5]]), 11);

    assert_eq!(
        path_sum_four_ways(&vec![vec![1, 1, 9, 1, 1, 1], vec![9, 1, 1, 1, 9, 1]]),
        9
    );

    let maze = vec![
        vec![1, 1, 1],
        vec![9, 9, 1],
        vec![1, 1, 1],
        vec![1, 9, 9],
        vec![1, 1, 1],
    ];
    assert_eq!(path_sum_four_ways(&maze), 11);

    //assert_eq!(path_sum_four_ways(&vec![vec![1, 1, 9], vec![9, 1, 1]]), 4);
    //assert_eq!(path_sum_four_ways(&vec![vec![9, 1, 1], vec![1, 1, 9]]), 4);
}
