//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::Sieve;

pub const INFO: EulerInfo = EulerInfo {
    number: 10,
    title: "Summation of primes",
    slug: "summation-of-primes",
    solve: Some(solve_default),
    solution: Some("$2b$04$8DAPZ/Z.HvaJk40N2VyY8OTXJrc5fQ7/c7a3v7xurMemfOFSy8iV2"),
};

pub fn solve_default() -> String {
    solve(2_000_000).to_string()
}

pub fn solve(n: u64) -> u64 {
    Sieve::new(n as usize).into_iter().sum()
}

#[test]
fn test_solve() {
    assert!(solve(10) == 2 + 3 + 5 + 7);
    assert!(solve(20) == 2 + 3 + 5 + 7 + 11 + 13 + 17 + 19);
    assert!(solve(30) == 2 + 3 + 5 + 7 + 11 + 13 + 17 + 19 + 23 + 29);
}
