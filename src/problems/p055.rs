//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use crate::util::{IsPalindrome, Reverse};

pub const INFO: EulerInfo = EulerInfo {
    number: 55,
    title: "Lychrel numbers",
    slug: "lychrel-numbers",
    solve: Some(solve_default),
    solution: Some("$2a$10$L4nBchpP.tfJ3XoqIFQz0OtJFaquEAYAwmNtPbZ.DhjlexPc2yoVe"),
};

pub fn reverse_add_palindrome(num: i128, limit: usize) -> Option<(usize, i128)> {
    let mut cur = num;
    for iter in 0..limit {
        cur = cur + cur.reverse(10);

        if cur.is_palindrome(10) {
            return Some((iter, cur));
        }
    }

    None
}

pub fn solve_default() -> String {
    solve().to_string()
}

pub fn solve() -> usize {
    lyrchel(10_000, 50).count()
}

pub fn lyrchel(max: i128, limit: usize) -> Box<dyn Iterator<Item = i128>> {
    Box::new((0..max).filter(move |i| reverse_add_palindrome(*i, limit) == None))
}

#[test]
fn test_solve() {
    assert_eq!(reverse_add_palindrome(47, 50), Some((0, 121)));
    assert_eq!(reverse_add_palindrome(349, 50), Some((2, 7337)));
    assert_eq!(
        reverse_add_palindrome(10677, 55),
        Some((52, 4668731596684224866951378664))
    );
}
