//! # Problem 1: Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or
//! 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. *Find the sum
//! of all the multiples of 3 or 5 below 1000*.
use crate::info::EulerInfo;
use std::f64;

pub const INFO: EulerInfo = EulerInfo {
    number: 25,
    title: "1000-digit Fibonacci number",
    slug: "1000-digit-fibonacci-number",
    solve: Some(solve_default),
    solution: Some("$2b$04$lBvh2ddXMHB85NM1jS1uDeZUAP51tDZL3Hg.uah.ANfagzMzEvJdi"),
};

pub fn solve_default() -> String {
    solve(1000).to_string()
}

pub fn solve(max: u32) -> u32 {
    let phi = (1.0 + (5.0 as f64).sqrt()) / 2.0;

    let sum1 = (1.0 / 2.0) * (5.0 as f64).log(phi);
    let sum2 = ((max - 1) as f64) * (10.0 as f64).log(phi);
    (sum1 + sum2).ceil() as u32
}

#[test]
fn test_solve() {
    assert_eq!(solve(2), 7);
    assert_eq!(solve(3), 12);
    assert_eq!(solve(4), 17);
    assert_eq!(solve(5), 21);
    assert_eq!(solve(6), 26);
    assert_eq!(solve(7), 31);
    assert_eq!(solve(8), 36);
}
